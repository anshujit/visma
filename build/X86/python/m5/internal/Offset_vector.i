%module(package="m5.internal") Offset_vector
%{
#include "params/Offset.hh"
%}

%include "std_container.i"

%import "python/m5/internal/param_Offset.i"

%include "std_vector.i"

%template(vector_Offset) std::vector< Offset * >;
