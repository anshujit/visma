#include "sim/init.hh"

namespace {

const uint8_t data_m5_internal_param_DirectedGenerator[] = {
    120,156,189,88,91,115,219,198,21,62,11,128,148,72,221,40,
    203,146,124,145,45,218,137,109,218,113,164,54,141,226,204,68,
    241,212,77,210,78,51,19,197,5,211,177,195,100,138,66,196,
    74,4,69,2,28,96,105,153,30,233,37,242,180,121,235,83,
    127,66,31,250,111,250,143,218,115,206,2,32,68,74,137,102,
    82,202,17,55,7,139,197,217,115,249,206,101,183,9,201,191,
    2,254,126,91,5,136,239,27,0,30,254,9,232,0,116,5,
    52,4,8,41,192,187,10,7,5,136,62,4,175,0,111,1,
    26,6,72,3,78,144,48,225,59,3,130,89,254,166,8,29,
    147,103,4,12,202,32,45,104,20,224,69,176,8,150,44,194,
    65,25,162,191,130,16,34,16,240,210,155,2,111,26,222,34,
    119,36,74,204,112,26,104,178,204,147,37,240,102,120,178,12,
    222,44,19,51,48,168,128,156,133,198,28,45,107,204,35,219,
    71,200,118,129,217,254,135,216,122,248,102,25,188,121,90,142,
    114,125,75,43,45,90,201,251,45,48,151,74,42,229,34,52,
    174,164,244,82,142,190,154,163,151,115,244,74,142,94,205,209,
    215,114,244,245,28,125,35,71,223,204,209,107,57,250,86,142,
    190,157,163,215,115,116,53,71,223,201,209,119,115,244,59,57,
    250,93,166,209,82,87,160,125,15,218,247,161,253,0,246,208,
    121,139,153,85,106,32,77,104,63,132,198,67,144,248,87,131,
    19,244,175,119,37,247,197,35,254,98,41,251,226,61,254,226,
    49,52,30,131,196,191,247,244,23,69,168,215,86,16,51,254,
    127,241,95,77,32,165,102,113,120,37,163,216,15,3,199,15,
    246,66,223,160,247,69,26,8,97,77,26,166,18,168,125,70,
    80,251,55,48,206,60,35,129,218,49,32,99,65,186,116,12,
    56,102,226,216,128,65,13,142,4,180,45,240,76,56,194,109,
    10,36,192,190,128,19,3,190,55,105,193,49,142,22,2,226,
    54,88,74,227,172,205,128,208,156,166,224,184,0,71,5,168,
    191,60,50,104,226,160,4,209,191,224,205,26,51,157,102,166,
    6,28,225,104,193,137,5,199,69,120,129,139,112,170,93,34,
    245,197,203,35,212,20,103,234,53,11,165,221,201,169,75,170,
    120,126,20,184,93,169,174,33,237,244,220,200,237,58,159,251,
    145,108,42,233,253,65,6,50,114,85,24,213,202,233,234,48,
    222,232,185,170,101,243,231,38,217,165,219,83,204,54,12,164,
    154,65,98,207,15,60,167,27,122,253,142,84,211,196,211,217,
    243,59,210,113,248,229,31,187,189,48,82,95,68,81,24,217,
    100,90,158,236,132,110,246,5,25,182,217,9,99,89,163,221,
    120,27,155,216,43,90,189,215,99,142,36,0,139,76,31,123,
    50,110,70,126,79,161,199,52,71,90,77,220,106,228,43,30,
    226,29,28,54,91,97,87,110,186,65,220,234,183,125,181,185,
    47,187,91,206,94,188,185,219,247,59,222,230,203,143,63,218,
    236,13,84,43,12,54,187,91,155,126,160,36,218,164,179,121,
    142,53,54,112,233,21,226,123,232,239,59,62,107,228,180,100,
    167,39,163,57,154,189,65,123,138,138,152,21,69,97,138,154,
    152,67,170,128,63,83,172,25,51,98,199,39,157,154,164,39,
    193,202,202,3,137,188,43,224,192,128,104,141,96,210,198,63,
    65,126,69,176,212,233,157,193,239,254,68,198,208,179,109,147,
    156,175,39,143,24,90,136,49,92,185,77,222,14,128,241,81,
    128,118,17,52,110,16,110,26,72,209,128,70,92,78,108,12,
    100,110,65,252,79,64,227,34,98,142,32,65,211,137,9,34,
    168,128,42,83,18,194,217,21,220,240,7,6,100,189,70,226,
    239,48,32,84,203,143,195,195,128,205,78,52,135,80,29,45,
    243,124,240,245,110,27,13,23,175,227,196,183,97,191,218,116,
    131,32,84,85,215,243,170,174,82,145,191,219,87,50,174,170,
    176,122,47,174,145,39,237,197,20,83,25,191,65,47,197,16,
    249,27,49,164,31,60,191,169,240,97,137,31,216,11,177,84,
    136,135,86,232,197,56,79,44,246,165,178,73,72,69,70,14,
    89,16,134,139,67,75,105,123,92,55,143,207,207,82,73,24,
    147,181,98,138,160,88,118,246,84,153,193,232,198,177,195,146,
    208,60,227,142,24,191,114,59,125,201,220,99,228,135,2,17,
    169,101,152,44,242,56,78,83,165,89,147,32,12,188,1,10,
    230,55,31,208,158,215,24,127,179,140,192,101,68,223,20,142,
    69,252,127,81,172,24,77,43,193,92,49,197,29,37,64,5,
    236,117,145,56,30,49,120,130,201,166,102,112,182,96,101,56,
    14,239,18,69,31,219,107,52,220,162,225,54,13,235,169,190,
    19,83,122,110,84,233,39,180,145,193,154,178,78,228,18,51,
    213,201,59,21,75,215,135,177,132,217,176,78,49,97,80,228,
    12,99,194,162,204,25,61,165,17,151,114,180,153,16,127,67,
    121,154,98,135,153,81,152,32,224,137,26,134,1,91,200,174,
    144,230,211,41,130,109,130,101,30,155,251,57,108,218,228,28,
    6,166,125,61,77,127,14,173,208,144,180,111,18,171,194,25,
    38,174,210,112,103,226,118,30,130,107,127,12,92,159,208,158,
    149,4,92,115,12,170,50,254,42,70,211,76,140,159,85,197,
    165,17,80,17,162,172,51,16,117,159,40,115,92,221,203,2,
    83,162,228,239,115,96,34,185,140,188,46,36,192,96,149,84,
    200,195,104,21,107,251,139,96,21,203,181,193,229,250,87,92,
    174,185,228,115,179,166,147,177,201,249,88,19,5,178,197,158,
    9,43,73,25,142,75,56,246,162,240,245,160,26,238,85,21,
    43,75,185,115,251,94,188,113,47,254,4,179,98,245,41,231,
    35,157,23,117,230,139,100,143,50,23,125,250,197,235,166,228,
    178,199,79,142,163,19,149,195,73,203,73,202,41,34,106,153,
    44,105,164,38,230,148,29,171,136,50,245,100,141,92,206,140,
    76,50,127,73,187,148,217,194,166,88,69,244,148,5,139,226,
    232,212,204,125,21,191,197,223,239,200,234,164,174,4,234,220,
    237,186,22,148,117,32,109,236,199,167,16,50,41,13,236,77,
    100,249,231,20,25,197,33,50,232,103,166,40,255,59,112,171,
    41,224,111,64,190,71,23,39,40,207,130,130,156,189,68,203,
    255,2,28,14,103,84,120,206,41,117,170,234,188,2,83,77,
    252,132,151,234,130,255,37,252,152,139,165,180,44,155,73,51,
    153,47,203,86,150,143,24,52,23,42,189,214,233,196,69,94,
    105,185,49,45,211,217,104,24,158,195,92,159,117,127,152,141,
    39,134,160,105,205,223,33,81,190,31,226,135,10,219,77,177,
    100,228,80,241,107,26,62,200,0,33,210,185,73,72,181,14,
    231,151,95,71,231,249,239,104,107,139,133,93,152,82,100,218,
    49,78,25,224,11,41,224,63,200,0,47,185,46,189,229,115,
    5,141,6,57,250,196,16,120,40,197,70,140,206,128,22,200,
    2,52,138,20,26,220,47,139,36,114,68,154,179,40,195,157,
    42,122,108,147,29,109,173,204,215,218,141,52,188,158,108,46,
    32,79,110,119,220,238,174,231,62,125,67,123,208,70,205,52,
    150,140,84,234,74,94,106,138,3,113,158,224,252,184,149,74,
    255,106,178,121,224,35,100,153,73,205,168,247,194,38,7,255,
    55,45,89,237,202,238,46,30,24,91,126,175,186,215,113,247,
    217,23,102,162,213,215,169,86,138,157,57,218,56,196,143,104,
    12,171,205,48,192,148,220,111,226,126,85,79,226,249,73,122,
    213,247,171,156,207,171,126,92,117,119,241,173,219,84,26,215,
    167,99,146,123,82,55,218,143,185,253,60,56,36,114,242,190,
    116,240,108,236,99,7,126,4,89,221,212,199,181,44,61,115,
    111,173,195,4,75,28,158,140,212,64,167,37,106,34,236,13,
    26,30,194,165,100,241,15,145,229,128,120,147,129,138,226,166,
    81,50,212,234,89,33,249,156,120,196,227,129,25,92,36,48,
    245,61,17,46,144,69,104,79,241,56,77,121,188,81,74,39,
    203,60,206,240,228,108,26,200,115,60,57,15,141,133,244,78,
    170,66,65,93,252,165,65,205,177,49,249,168,56,249,191,198,
    178,253,228,114,133,182,63,134,164,136,159,23,199,167,250,192,
    103,58,142,181,237,177,212,15,174,178,130,250,40,193,10,138,
    23,193,13,108,8,45,110,8,183,169,33,60,226,166,209,49,
    116,79,56,116,25,119,248,124,91,65,225,28,200,195,113,113,
    53,28,117,239,71,120,112,123,61,25,120,246,35,200,183,115,
    252,122,178,38,163,128,253,17,114,213,215,20,87,177,127,27,
    135,39,101,162,156,138,12,195,66,6,200,181,75,241,237,63,
    82,223,214,22,79,165,35,123,155,134,202,169,220,163,237,255,
    238,249,169,192,9,250,93,167,217,235,199,116,76,184,216,66,
    108,9,56,65,166,19,234,238,79,124,21,15,98,37,187,204,
    252,2,203,136,53,223,54,240,35,187,69,81,67,230,201,142,
    84,242,60,252,40,210,57,57,135,122,18,235,72,56,192,35,
    2,119,222,248,220,113,156,75,200,192,159,34,203,31,136,247,
    188,206,192,120,102,92,22,203,70,169,88,18,92,200,70,238,
    88,181,48,239,67,218,97,14,98,155,163,117,33,243,25,223,
    1,166,85,133,60,203,39,160,29,183,171,47,113,248,158,194,
    126,7,146,243,165,253,32,115,59,29,185,185,173,215,71,38,
    140,59,46,178,92,83,237,223,208,60,157,127,187,91,27,169,
    90,27,90,173,58,219,156,175,32,187,91,28,178,249,53,146,
    188,253,149,236,134,209,224,171,208,147,106,109,228,253,51,207,
    139,108,55,216,151,206,43,73,53,94,221,25,93,144,20,120,
    205,35,93,85,61,83,148,211,107,199,100,209,139,240,165,190,
    123,227,110,117,252,253,103,157,176,121,32,189,100,205,173,243,
    215,124,30,118,93,156,63,123,151,186,159,238,178,56,242,222,
    139,232,171,229,145,217,88,70,190,219,241,223,72,117,253,44,
    11,62,63,140,234,216,80,143,219,79,239,246,60,60,148,17,
    217,183,195,209,146,95,48,124,197,12,206,51,243,40,31,189,
    219,57,186,245,119,19,167,159,109,65,172,24,81,215,237,36,
    246,25,149,56,179,76,42,202,237,159,98,162,149,34,80,167,
    239,21,97,125,212,200,4,217,236,137,219,26,102,59,22,114,
    28,236,145,220,247,81,252,136,57,101,95,37,53,229,211,159,
    75,124,121,14,147,79,16,250,120,160,47,44,158,210,133,24,
    159,148,233,246,178,180,80,18,69,131,74,142,41,202,88,116,
    44,115,182,82,178,102,103,74,86,105,202,228,235,167,57,60,
    8,150,173,210,204,172,184,200,127,235,152,114,202,198,250,124,
    73,252,15,103,183,120,171,
};

EmbeddedPython embedded_m5_internal_param_DirectedGenerator(
    "m5/internal/param_DirectedGenerator.py",
    "/home/anshujit/gem5_fs/build/X86/python/m5/internal/param_DirectedGenerator.py",
    "m5.internal.param_DirectedGenerator",
    data_m5_internal_param_DirectedGenerator,
    2375,
    7220);

} // anonymous namespace
