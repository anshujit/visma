%module(package="m5.internal") param_DeviceVirtualizer

%{
#include "sim/sim_object.hh"
#include "params/DeviceVirtualizer.hh"
#include <cstddef>
#include "base/types.hh"
#include <vector>
#include "params/IdeDisk.hh"
#include <cstddef>
#include "params/DmaVirtualizer.hh"
#include <vector>
#include "base/types.hh"
#include <vector>
#include "base/types.hh"
#include <vector>
#include "params/Offset.hh"
#include "sys_conf/device_virtualizer.hh"
/**
  * This is a workaround for bug in swig. Prior to gcc 4.6.1 the STL
  * headers like vector, string, etc. used to automatically pull in
  * the cstddef header but starting with gcc 4.6.1 they no longer do.
  * This leads to swig generated a file that does not compile so we
  * explicitly include cstddef. Additionally, including version 2.0.4,
  * swig uses ptrdiff_t without the std:: namespace prefix which is
  * required with gcc 4.6.1. We explicitly provide access to it.
  */
#include <cstddef>
using std::ptrdiff_t;
%}

%import "stdint.i"
%import "base/types.hh"
%import "IdeDisk_vector.i"
%import "python/m5/internal/param_DmaVirtualizer.i"
%import "Unsigned_vector.i"
%import "Unsigned_vector.i"
%import "Offset_vector.i"

%import "python/m5/internal/param_MemObject.i"


// stop swig from creating/wrapping default ctor/dtor
%nodefault DeviceVirtualizer;
class DeviceVirtualizer
    : public MemObject
{
  public:
};

%include "params/DeviceVirtualizer.hh"
