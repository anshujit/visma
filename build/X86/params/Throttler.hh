#ifndef __PARAMS__Throttler__
#define __PARAMS__Throttler__

class Throttler;


#include "params/MemObject.hh"

struct ThrottlerParams
    : public MemObjectParams
{
    Throttler * create();
    unsigned int port_mem_side_connection_count;
    unsigned int port_data_port_connection_count;
};

#endif // __PARAMS__Throttler__
