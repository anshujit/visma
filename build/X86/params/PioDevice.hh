#ifndef __PARAMS__PioDevice__
#define __PARAMS__PioDevice__

class PioDevice;

#include <cstddef>
#include "params/System.hh"
#include <cstddef>
#include "base/types.hh"

#include "params/MemObject.hh"

struct PioDeviceParams
    : public MemObjectParams
{
    System * system;
    int tileID;
    unsigned int port_pio_connection_count;
};

#endif // __PARAMS__PioDevice__
