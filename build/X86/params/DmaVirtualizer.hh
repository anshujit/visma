#ifndef __PARAMS__DmaVirtualizer__
#define __PARAMS__DmaVirtualizer__

class DmaVirtualizer;

#include <vector>
#include "base/types.hh"

#include "params/MemObject.hh"

struct DmaVirtualizerParams
    : public MemObjectParams
{
    DmaVirtualizer * create();
    std::vector< unsigned > maxdmapkts;
    unsigned int port_master_connection_count;
    unsigned int port_slave_connection_count;
};

#endif // __PARAMS__DmaVirtualizer__
