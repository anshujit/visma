#ifndef __PARAMS__IdeDisk__
#define __PARAMS__IdeDisk__

class IdeDisk;

#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "enums/IdeID.hh"
#include <cstddef>
#include "params/DiskImage.hh"
#include <cstddef>
#include "base/types.hh"
#include <cstddef>
#include "base/types.hh"

#include "params/SimObject.hh"

#include "enums/IdeID.hh"

struct IdeDiskParams
    : public SimObjectParams
{
    IdeDisk * create();
    Tick delay;
    int dma_offset;
    Enums::IdeID driveID;
    DiskImage * image;
    unsigned maxbytecount;
    int tileID;
};

#endif // __PARAMS__IdeDisk__
