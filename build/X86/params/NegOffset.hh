#ifndef __PARAMS__NegOffset__
#define __PARAMS__NegOffset__

class NegOffset;

#include <cstddef>
#include "base/types.hh"

#include "params/MemObject.hh"

struct NegOffsetParams
    : public MemObjectParams
{
    NegOffset * create();
    int tileID;
    unsigned int port_io_port_connection_count;
    unsigned int port_ruby_port_connection_count;
};

#endif // __PARAMS__NegOffset__
