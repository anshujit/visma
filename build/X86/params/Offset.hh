#ifndef __PARAMS__Offset__
#define __PARAMS__Offset__

class Offset;

#include <cstddef>
#include "base/types.hh"

#include "params/MemObject.hh"

struct OffsetParams
    : public MemObjectParams
{
    Offset * create();
    int tileID;
    unsigned int port_io_port_connection_count;
    unsigned int port_ruby_port_connection_count;
};

#endif // __PARAMS__Offset__
