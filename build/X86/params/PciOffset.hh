#ifndef __PARAMS__PciOffset__
#define __PARAMS__PciOffset__

class PciOffset;

#include <cstddef>
#include "base/types.hh"

#include "params/MemObject.hh"

struct PciOffsetParams
    : public MemObjectParams
{
    PciOffset * create();
    int tileID;
    unsigned int port_io_port_connection_count;
    unsigned int port_bus_port_connection_count;
};

#endif // __PARAMS__PciOffset__
