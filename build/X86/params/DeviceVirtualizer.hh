#ifndef __PARAMS__DeviceVirtualizer__
#define __PARAMS__DeviceVirtualizer__

class DeviceVirtualizer;

#include <cstddef>
#include "base/types.hh"
#include <vector>
#include "params/IdeDisk.hh"
#include <cstddef>
#include "params/DmaVirtualizer.hh"
#include <vector>
#include "base/types.hh"
#include <vector>
#include "base/types.hh"
#include <vector>
#include "params/Offset.hh"

#include "params/MemObject.hh"

struct DeviceVirtualizerParams
    : public MemObjectParams
{
    DeviceVirtualizer * create();
    unsigned cycle_period;
    std::vector< IdeDisk * > disks;
    DmaVirtualizer * dmavirt;
    std::vector< unsigned > maxcontrolpkts;
    std::vector< unsigned > maxdiskbytes;
    std::vector< Offset * > offsets;
    unsigned int port_master_connection_count;
    unsigned int port_slave_connection_count;
};

#endif // __PARAMS__DeviceVirtualizer__
