/*
 * Copyright (c) 2017 Jason Lowe-Power
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Jason Lowe-Power
 */

#include "sys_conf/dma_virtualizer.hh"

#include "debug/DmaVirtualizer.hh"

DmaVirtualizer::DmaVirtualizer(DmaVirtualizerParams *params) :
    MemObject(params)
    //instPort(params->name + ".inst_port", this),
    //offsets(params->offsets),
    //busport(params->name + ".slave", this),
    //ioport(params->name + ".io_port", this),
    //tileID(params->tileID),
    //blocked(false),
{
    // create the slave ports based on the number of connected ports
    for (size_t i = 0; i < params->port_slave_connection_count; ++i) {
        busport.push_back(new BusSidePort(csprintf("%s.slave%d", name(),
            i), this));
    }

    //offsets = safe_cast<std::vector<Offset *>> (params->offsets);
    // create the master ports based on the number of connected ports
    for (size_t i = 0; i < params->port_master_connection_count; ++i) {
        ioport.push_back(new IOSidePort(csprintf("%s.master%d", name(),
            i), this));
    }

    maxdmapkts = params->maxdmapkts;

    for (size_t i = 0; i < busport.size(); i++) {
        count.push_back(0);
        blocked.push_back(false);
    }
}

BaseMasterPort&
DmaVirtualizer::getMasterPort(const std::string& if_name, PortID idx)
{
    //panic_if(idx != InvalidPortID, "This object doesn't support vector ports");

    //if (if_name != "master") {
        // pass it along to our super class
    //    return MemObject::getMasterPort(if_name, idx);
    if (if_name == "master") {
        if (idx >= static_cast<PortID>(ioport.size())) {
            panic("DmaVirtualizer::getMasterPort: unknown index %d\n", idx);
        }

        return *ioport[idx];
    } else
        return MemObject::getMasterPort(if_name, idx);
        

    // This is the name from the Python SimObject declaration (DmaVirtualizer.py)
    //if (if_name == "io_port") {
    //    return ioport;
    //} else {
        // pass it along to our super class
    //    return MemObject::getMasterPort(if_name, idx);
    //}
}

BaseSlavePort&
DmaVirtualizer::getSlavePort(const std::string& if_name, PortID idx)
{
    //panic_if(idx != InvalidPortID, "This object doesn't support vector ports");

    // This is the name from the Python SimObject declaration in SimpleCache.py
    //if (if_name == "inst_port") {
    //    return instPort;
    if (if_name == "slave") {
        if (idx >= static_cast<PortID>(busport.size())) {
            panic("DmaVirtualizer::getSlavePort: unknown index %d\n", idx);
        }

        return *busport[idx];
    //} else
    //    return MemObject::getSlavePort(if_name, idx);

    //if (if_name == "slave") {
    //    return busport;
    } else {
        // pass it along to our super class
        return MemObject::getSlavePort(if_name, idx);
    }
}

bool
DmaVirtualizer::BusSidePort::sendPacket(PacketPtr pkt)
{
    // Note: This flow control is very simple since the memobj is blocking.

    //panic_if(blockedPacket != nullptr, "Should never try to send if blocked!");
    if(blockedPacket)
        return false;
    // If we can't send the packet across the port, store it for later.
    else if (!sendTimingResp(pkt)) {
        blockedPacket = pkt;
    } else {
        for (size_t i = 0; i < owner->ioport.size(); ++i)
            owner->ioport[i]->trySendRetry();
    }

    return true;
}

AddrRangeList
DmaVirtualizer::BusSidePort::getAddrRanges() const
{
    return owner->getAddrRanges();
}

void
DmaVirtualizer::BusSidePort::trySendRetry()
{
    if (needRetry && blockedPacket == nullptr) {
        // Only send a retry if the port is now completely free
        needRetry = false;
        DPRINTF(DmaVirtualizer, "Sending retry req for %d\n", id);
        sendRetryReq();
    }
}

void
DmaVirtualizer::BusSidePort::recvFunctional(PacketPtr pkt)
{
    // Just forward to the memobj.
    return owner->handleFunctional(pkt);
}

bool
DmaVirtualizer::BusSidePort::recvTimingReq(PacketPtr pkt)
{
    // Just forward to the memobj.
    if (!owner->handleRequest(pkt)) {
        needRetry = true;
        return false;
    } else {
        return true;
    }
}

void
DmaVirtualizer::BusSidePort::recvRespRetry()
{
    // We should have a blocked packet if this function is called.
    assert(blockedPacket != nullptr);

    // Grab the blocked packet.
    PacketPtr pkt = blockedPacket;
    blockedPacket = nullptr;

    // Try to resend it. It's possible that it fails again.
    sendPacket(pkt);
    //    blockedPacket = pkt;
    //owner->IOPortRetryResp();
}

bool
DmaVirtualizer::IOSidePort::sendPacket(PacketPtr pkt)
{
    // Note: This flow control is very simple since the memobj is blocking.

    //panic_if(blockedPacket != nullptr, "Should never try to send if blocked!");
    // If we can't send the packet across the port, store it for later.
    if(blockedPacket)
        return false;
    else if (!sendTimingReq(pkt)) {
        blockedPacket = pkt;
    } else {
        for(int i = 0; i < owner->busport.size(); i++)
            owner->busport[i]->trySendRetry();
    }
    return true;
}

void
DmaVirtualizer::IOSidePort::trySendRetry()
{
    if (needRetry && blockedPacket == nullptr) {
        // Only send a retry if the port is now completely free
        needRetry = false;
        DPRINTF(DmaVirtualizer, "Sending Resp for %d\n", id);
        sendRetryResp();
    }
}


bool
DmaVirtualizer::IOSidePort::recvTimingResp(PacketPtr pkt)
{
    // Just forward to the memobj.
    if(!owner->handleResponse(pkt)) {
        needRetry = true;
        return false;
    } else
        return true;
}

void
DmaVirtualizer::IOSidePort::recvReqRetry()
{
    // We should have a blocked packet if this function is called.
    assert(blockedPacket != nullptr);

    // Grab the blocked packet.
    PacketPtr pkt = blockedPacket;
    blockedPacket = nullptr;

    // Try to resend it. It's possible that it fails again.
    sendPacket(pkt);
    //    blockedPacket = pkt;
    //owner->RubyPortRetryReq();
}

void
DmaVirtualizer::IOSidePort::recvRangeChange()
{
    owner->sendRangeChange();
}

void
DmaVirtualizer::reset()
{
    for(int i = 0; i < count.size(); i++) {
        count[i] = 0;
    //    for(int j = 0; j < ioswitch[i].size(); j++)
    //        ioswitch[i][j]->unblock();
        blocked[i] = false;
    }
    //busport.trySendRetry();
    for(int i = 0; i < busport.size(); i++)
        busport[i]->trySendRetry();
}

bool
DmaVirtualizer::handleRequest(PacketPtr pkt)
{
    //if (blocked) {
    //    // There is currently an outstanding request. Stall.
    //    return false;
    //}
    int tileID = pkt->req->_tileId;
    assert(tileID != -1);
    
    if (blocked[tileID])
        return false;

    DPRINTF(DmaVirtualizer, "Got request for addr %#x\n", pkt->getAddr());
    //inform("Tile:%d, Got request for addr %#x\n", tileID, pkt->getAddr());
    // This memobj is now blocked waiting for the response to this packet.
    //blocked = true;
    //uint8_t *pkt_data(pkt->getPtr<uint8_t>());
    
    //if(!ioport[tileID]->sendPacket(pkt))
    //    return false;
    if(!ioport[0]->sendPacket(pkt))
        return false;
    
    count[tileID]++;
    if(count[tileID] == maxdmapkts[tileID]) {
    //    for(int i = 0; i < ioswitch[tileID].size(); i++)
    //        ioswitch[tileID][i]->block();
    //inform("ioswitch blocked\n");
        blocked[tileID] = true;
    }
    return true;
}

bool
DmaVirtualizer::handleResponse(PacketPtr pkt)
{
    //assert(blocked);
    DPRINTF(DmaVirtualizer, "Got response for addr %#x\n", pkt->getAddr());

    int tileID = pkt->req->_tileId;
    // The packet is now done. We're about to put it in the port, no need for
    // this object to continue to stall.
    // We need to free the resource before sending the packet in case the CPU
    // tries to send another request immediately (e.g., in the same callchain).
    //blocked = false;
    // Simply forward to the memory port
    //if (pkt->req->isInstFetch()) {
    //    instPort.sendPacket(pkt);
    //} else {
    if(!busport[tileID]->sendPacket(pkt))
        return false;
    //}

    // For each of the cpu ports, if it needs to send a retry, it should do it
    // now since this memory object may be unblocked now.
    //instPort.trySendRetry();
    //rubyport.trySendRetry();

    return true;
}

void
DmaVirtualizer::handleFunctional(PacketPtr pkt)
{
    // Just pass this on to the memory side to handle for now.
    ioport[pkt->req->_tileId]->sendFunctional(pkt);
}

AddrRangeList
DmaVirtualizer::getAddrRanges() const
{
    DPRINTF(DmaVirtualizer, "Sending new ranges\n");
    // Just use the same ranges as whatever is on the memory side.
    AddrRangeList ranges;
    for (size_t i = 0; i < ioport.size(); ++i) {
        ranges.splice(ranges.begin(),
                      ioport[i]->getAddrRanges());
    }
    return ranges;
}

void
DmaVirtualizer::sendRangeChange()
{
    //instPort.sendRangeChange();
    for(int i = 0; i < busport.size(); i++)
        busport[i]->sendRangeChange();
}

DmaVirtualizer*
DmaVirtualizerParams::create()
{
    return new DmaVirtualizer(this);
}
