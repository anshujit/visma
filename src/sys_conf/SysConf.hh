#include "sys_conf/tile.hh"
#include "params/SysConf.hh"
#include "mem/ruby/network/garnet2.0/Router.hh"
#include "mem/ruby/network/garnet2.0/NetworkInterface.hh"
#include "mem/ruby/network/garnet2.0/connBits.hh"
#include "mem/ruby/network/garnet2.0/LUT.hh"

#include "sim/sim_object.hh"
#include "sys_conf/device_arbiter.hh"
#include "mem/modi_noncoherent_xbar.hh"
//#include "sim/eventq.hh"
#include <string>
#include <map>

//class tile;
//class DeviceArbiter;
//class Throttler;
class SysConf : public SimObject
{

	public:
		typedef SysConfParams Params;
    	SysConf(const Params *p);
		//~SysConf();
		std::vector<int> releaseTile(int id);
		std::vector<int> requestTile(int num_cores, int max_pkts);
		//EventWrapper<SysConf, &SysConf::requestTile> requestTileEvent;
		bool isEnabled() const { return enable; }
        void nextTileId();
        tile* getTile(int id);

	private:
		
        int _tileID;
		bool enable;
		int m_nodes;
		std::vector<NetworkInterface *> m_nis;
		std::vector<connBits *> m_conns;
		std::vector<Router *> m_routers;
		std::vector<LUT *> m_luts;
		int m_rows;
		int m_cols;
		std::string tileType; 
		std::vector<std::vector<char>> links;
		std::vector<std::vector<int>> nbrs;
		int m_dma;
		DeviceArbiter *arbiter;
        int numa_high_bit;
        ModiNoncoherentXBar *ioXBar;
        //std::vector<Throttler*> throttlers;
        //Addr io_startAddr;
        //Addr curr_startAddr;

		std::vector<std::vector<int>> routers;
		tile *m_tile;
		//std::vector<std::pair<int, tile*>> tiles;
        //std::map<int, tile *>::iterator it;
        std::map<int, tile *> tiles;

};
