/*
 * Copyright (c) 2017 Jason Lowe-Power
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Jason Lowe-Power
 */

#include "sys_conf/device_virtualizer.hh"
#include "dev/storage/ide_disk.hh"
#include "debug/DeviceVirtualizer.hh"

DeviceVirtualizer::DeviceVirtualizer(DeviceVirtualizerParams *params) :
    MemObject(params),
    //instPort(params->name + ".inst_port", this),
    //offsets(params->offsets),
    //busport(params->name + ".slave", this),
    //ioport(params->name + ".io_port", this),
    //tileID(params->tileID),
    //blocked(false),
    resetEvent(this, false)
{
    // create the slave ports based on the number of connected ports
    for (size_t i = 0; i < params->port_slave_connection_count; ++i) {
        busport.push_back(new BusSidePort(csprintf("%s.slave%d", name(),
            i), this));
    }

    //offsets = safe_cast<std::vector<Offset *>> (params->offsets);
    // create the master ports based on the number of connected ports
    for (size_t i = 0; i < params->port_master_connection_count; ++i) {
        ioport.push_back(new IOSidePort(csprintf("%s.master%d", name(),
            i), this));
    }

    disks = params->disks;        
    maxcontrolpkts = params->maxcontrolpkts;
    period = params->cycle_period;
    maxdiskbytes = params->maxdiskbytes;
    dmavirt = params->dmavirt;

    for (int i = 0; i < disks.size(); i++) {
        disks[i]->setDevVirt(this);
    }
    for (size_t i = 0; i < params->offsets.size(); i++) {
        int tileID = params->offsets[i]->getTileID();
        offsets[tileID].push_back(params->offsets[i]);
    }

    for (size_t i = 0; i < ioport.size(); i++) {
        count.push_back(0);
        blocked.push_back(false);
    }

    for(std::vector<IdeDisk *>::iterator disk = disks.begin(); disk != disks.end(); disk++) {
        diskwrite[*disk] = false;
        diskread[*disk] = false;
        dmaTransfer[*disk] = false;
    }

    schedule(resetEvent, curTick());
}

BaseMasterPort&
DeviceVirtualizer::getMasterPort(const std::string& if_name, PortID idx)
{
    //panic_if(idx != InvalidPortID, "This object doesn't support vector ports");

    //if (if_name != "master") {
        // pass it along to our super class
    //    return MemObject::getMasterPort(if_name, idx);
    if (if_name == "master") {
        if (idx >= static_cast<PortID>(ioport.size())) {
            panic("DeviceVirtualizer::getMasterPort: unknown index %d\n", idx);
        }

        return *ioport[idx];
    } else
        return MemObject::getMasterPort(if_name, idx);
        

    // This is the name from the Python SimObject declaration (DeviceVirtualizer.py)
    //if (if_name == "io_port") {
    //    return ioport;
    //} else {
        // pass it along to our super class
    //    return MemObject::getMasterPort(if_name, idx);
    //}
}

BaseSlavePort&
DeviceVirtualizer::getSlavePort(const std::string& if_name, PortID idx)
{
    //panic_if(idx != InvalidPortID, "This object doesn't support vector ports");

    // This is the name from the Python SimObject declaration in SimpleCache.py
    //if (if_name == "inst_port") {
    //    return instPort;
    if (if_name == "slave") {
        if (idx >= static_cast<PortID>(busport.size())) {
            panic("DeviceVirtualizer::getSlavePort: unknown index %d\n", idx);
        }

        return *busport[idx];
    //} else
    //    return MemObject::getSlavePort(if_name, idx);

    //if (if_name == "slave") {
    //    return busport;
    } else {
        // pass it along to our super class
        return MemObject::getSlavePort(if_name, idx);
    }
}

bool
DeviceVirtualizer::BusSidePort::sendPacket(PacketPtr pkt)
{
    // Note: This flow control is very simple since the memobj is blocking.

    //panic_if(blockedPacket != nullptr, "Should never try to send if blocked!");
    if(blockedPacket)
        return false;
    // If we can't send the packet across the port, store it for later.
    else if (!sendTimingResp(pkt)) {
        blockedPacket = pkt;
    } else {
        for (size_t i = 0; i < owner->ioport.size(); ++i)
            owner->ioport[i]->trySendRetry();
    }

    return true;
}

AddrRangeList
DeviceVirtualizer::BusSidePort::getAddrRanges() const
{
    return owner->getAddrRanges();
}

void
DeviceVirtualizer::BusSidePort::trySendRetry()
{
    if (needRetry && blockedPacket == nullptr) {
        // Only send a retry if the port is now completely free
        needRetry = false;
        DPRINTF(DeviceVirtualizer, "Sending retry req for %d\n", id);
        sendRetryReq();
    }
}

void
DeviceVirtualizer::BusSidePort::recvFunctional(PacketPtr pkt)
{
    // Just forward to the memobj.
    return owner->handleFunctional(pkt);
}

bool
DeviceVirtualizer::BusSidePort::recvTimingReq(PacketPtr pkt)
{
    // Just forward to the memobj.
    if (!owner->handleRequest(pkt)) {
        needRetry = true;
        return false;
    } else {
        return true;
    }
}

void
DeviceVirtualizer::BusSidePort::recvRespRetry()
{
    // We should have a blocked packet if this function is called.
    assert(blockedPacket != nullptr);

    // Grab the blocked packet.
    PacketPtr pkt = blockedPacket;
    blockedPacket = nullptr;

    // Try to resend it. It's possible that it fails again.
    sendPacket(pkt);
    //    blockedPacket = pkt;
    //owner->IOPortRetryResp();
}

bool
DeviceVirtualizer::IOSidePort::sendPacket(PacketPtr pkt)
{
    // Note: This flow control is very simple since the memobj is blocking.

    //panic_if(blockedPacket != nullptr, "Should never try to send if blocked!");
    // If we can't send the packet across the port, store it for later.
    if(blockedPacket)
        return false;
    else if (!sendTimingReq(pkt)) {
        blockedPacket = pkt;
    } else {
        //inform("tileID:%d, Got request for addr %#x\n", pkt->req->_tileId, pkt->getAddr());
        for(int i = 0; i < owner->busport.size(); i++)
            owner->busport[i]->trySendRetry();
    }
    return true;
}

void
DeviceVirtualizer::IOSidePort::trySendRetry()
{
    if (needRetry && blockedPacket == nullptr) {
        // Only send a retry if the port is now completely free
        needRetry = false;
        DPRINTF(DeviceVirtualizer, "Sending Resp for %d\n", id);
        sendRetryResp();
    }
}


bool
DeviceVirtualizer::IOSidePort::recvTimingResp(PacketPtr pkt)
{
    // Just forward to the memobj.
    if(!owner->handleResponse(pkt)) {
        needRetry = true;
        return false;
    } else
        return true;
}

void
DeviceVirtualizer::IOSidePort::recvReqRetry()
{
    // We should have a blocked packet if this function is called.
    assert(blockedPacket != nullptr);

    // Grab the blocked packet.
    PacketPtr pkt = blockedPacket;
    blockedPacket = nullptr;

    // Try to resend it. It's possible that it fails again.
    sendPacket(pkt);
    //    blockedPacket = pkt;
    //owner->RubyPortRetryReq();
}

void
DeviceVirtualizer::IOSidePort::recvRangeChange()
{
    owner->sendRangeChange();
}

void
DeviceVirtualizer::ProcessReset()
{
    for(int i = 0; i < count.size(); i++) {
        count[i] = 0;
        for(int j = 0; j < offsets[i].size(); j++)
            offsets[i][j]->unblock();
        blocked[i] = false;
    }
    //busport.trySendRetry();
    for(std::vector<IdeDisk *>::iterator disk = disks.begin(); disk != disks.end(); disk++) {
        if(diskwrite[*disk]) {
            assert((dmaTransfer[*disk] == false) && (diskread[*disk] == false));
            diskwrite[*disk] = false;
            (*disk)->diskwritecontinue();
        } else if(diskread[*disk]) {
            assert((dmaTransfer[*disk] == false) && (diskwrite[*disk] == false));
            diskread[*disk] = false;
            (*disk)->diskreadcontinue();
        } else if(dmaTransfer[*disk]) {
            assert((diskwrite[*disk] == false) && (diskread[*disk] == false));
            dmaTransfer[*disk] = false;
            (*disk)->startdmaTransfer();
        }
    }

    for(int i = 0; i < busport.size(); i++)
        busport[i]->trySendRetry();

    dmavirt->reset();
    schedule(resetEvent, curTick() + period);
}

void
DeviceVirtualizer::diskwriteAgain(int tileID, IdeDisk *disk)
{
    diskread[disk] = false;
    dmaTransfer[disk] = false;
    //if(curbytesWritten < maxdiskbytes[tileID]) {
    //    disk->diskwritecontinue();
    //    diskwrite[disk] = false;
    //} else {
        diskwrite[disk] = true;
    //}
}

void
DeviceVirtualizer::diskreadAgain(int tileID, IdeDisk *disk)
{
    diskwrite[disk] = false;
    dmaTransfer[disk] = false;
    //if(curbytesRead < maxdiskbytes[tileID]) {
    //    disk->diskreadcontinue();
    //    diskread[disk] = false;
    //} else {
        diskread[disk] = true;
    //}
}

void
DeviceVirtualizer::needDmaTransfer(int tileID, IdeDisk *disk)
{
    diskwrite[disk] = false;
    diskread[disk] = false;
    dmaTransfer[disk] = true;
}

bool
DeviceVirtualizer::handleRequest(PacketPtr pkt)
{
    //if (blocked) {
    //    // There is currently an outstanding request. Stall.
    //    return false;
    //}
    int tileID = pkt->req->_tileId;
    assert(tileID != -1);
    
    if (blocked[tileID]) {
        //inform("Blocked request for addr %#x\n", pkt->getAddr());
        return false;
    }

    //inform("Got request for addr %#x\n", pkt->getAddr());
    DPRINTF(DeviceVirtualizer, "Got request for addr %#x\n", pkt->getAddr());
    //inform("Tile:%d, Got request for addr %#x\n", tileID, pkt->getAddr());
    // This memobj is now blocked waiting for the response to this packet.
    //blocked = true;
    //uint8_t *pkt_data(pkt->getPtr<uint8_t>());
    
    if(!ioport[tileID]->sendPacket(pkt))
        return false;

    count[tileID]++;
    if(count[tileID] == maxcontrolpkts[tileID]) {
        for(int i = 0; i < offsets[tileID].size(); i++)
            offsets[tileID][i]->block();
    //inform("ioswitch blocked\n");
        blocked[tileID] = true;
    }
    return true;
}

bool
DeviceVirtualizer::handleResponse(PacketPtr pkt)
{
    //assert(blocked);
    DPRINTF(DeviceVirtualizer, "Got response for addr %#x\n", pkt->getAddr());
    //inform("Got response for addr %#x\n", pkt->getAddr());
    //int tileID = pkt->req->_tileId;
    // The packet is now done. We're about to put it in the port, no need for
    // this object to continue to stall.
    // We need to free the resource before sending the packet in case the CPU
    // tries to send another request immediately (e.g., in the same callchain).
    //blocked = false;
    // Simply forward to the memory port
    //if (pkt->req->isInstFetch()) {
    //    instPort.sendPacket(pkt);
    //} else {
    //inform("pkt_addr:%x, tileID:%d\n", pkt->getAddr(), tileID);
    //if(!busport[tileID]->sendPacket(pkt))
    //    return false;
    //}
    if(!busport[0]->sendPacket(pkt))
        return false;

    // For each of the cpu ports, if it needs to send a retry, it should do it
    // now since this memory object may be unblocked now.
    //instPort.trySendRetry();
    //rubyport.trySendRetry();

    return true;
}

void
DeviceVirtualizer::handleFunctional(PacketPtr pkt)
{
    // Just pass this on to the memory side to handle for now.
    ioport[pkt->req->_tileId]->sendFunctional(pkt);
}

AddrRangeList
DeviceVirtualizer::getAddrRanges() const
{
    DPRINTF(DeviceVirtualizer, "Sending new ranges\n");
    // Just use the same ranges as whatever is on the memory side.
    AddrRangeList ranges;
    for (int i = 0; i < ioport.size(); ++i) {
        ranges.splice(ranges.begin(),
                      ioport[i]->getAddrRanges());
        //setRanges(i);
        //for (int j = 0; j < offsets[i].size(); j++) {
        //    offsets[i][j]->setDiskAddrRange(ioport[i]->getAddrRanges());
        //}
    }
    return ranges;
}

void
DeviceVirtualizer::sendRangeChange()
{
    //instPort.sendRangeChange();
    for(int i = 0; i < busport.size(); i++)
        busport[i]->sendRangeChange();
    for (int i = 0; i < ioport.size(); ++i) {
        for (int j = 0; j < offsets[i].size(); j++)
            offsets[i][j]->setDiskAddrRange(ioport[i]->getAddrRanges());
        //for(const auto M5_VAR_USED &r : ioport[i]->getAddrRanges())
        //    inform("%s\n", r.to_string());
    }
}

DeviceVirtualizer*
DeviceVirtualizerParams::create()
{
    return new DeviceVirtualizer(this);
}
