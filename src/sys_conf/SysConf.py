from m5.SimObject import SimObject
from m5.params import *

class SysConf(SimObject):
    type = 'SysConf'
    cxx_header = "sys_conf/SysConf.hh"

    network = Param.RubyNetwork(NULL,"The network to configure")
    num_rows = Param.Int(0,"Number of rows in the Mesh topology")
    enable = Param.Bool(False, "Enable/Disable the SysConf")
    arbiter = Param.DeviceArbiter(NULL, "IOArbiter of the system")
    numa_high_bit = Param.Int(0, "numa_high_bit")
    io_xbar = Param.ModiNoncoherentXBar(NULL, "Device xbar of the system")
    #throttlers = VectorParam.Throttler("Packet throttlers in the system")
    #io_startAddr = Param.Addr("Start address of the io device")

    @classmethod
    def export_methods(cls, code):
        code('''
    std::vector<int> requestTile(int num_cores, int max_pkts);
    std::vector<int> releaseTile(int id);
''')
