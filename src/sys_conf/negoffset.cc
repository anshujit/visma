/*
 * Copyright (c) 2017 Jason Lowe-Power
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Jason Lowe-Power
 */

#include "sys_conf/negoffset.hh"

#include "debug/NegOffset.hh"

NegOffset::NegOffset(NegOffsetParams *params) :
    MemObject(params),
    //instPort(params->name + ".inst_port", this),
    ioport(params->name + ".ruby_port", this),
    rubyport(params->name + ".io_port", this),
    tileID(params->tileID),
    blocked(false)
{
}

BaseMasterPort&
NegOffset::getMasterPort(const std::string& if_name, PortID idx)
{
    panic_if(idx != InvalidPortID, "This object doesn't support vector ports");

    // This is the name from the Python SimObject declaration (NegOffset.py)
    if (if_name == "ruby_port") {
        return rubyport;
    } else {
        // pass it along to our super class
        return MemObject::getMasterPort(if_name, idx);
    }
}

BaseSlavePort&
NegOffset::getSlavePort(const std::string& if_name, PortID idx)
{
    panic_if(idx != InvalidPortID, "This object doesn't support vector ports");

    // This is the name from the Python SimObject declaration in SimpleCache.py
    //if (if_name == "inst_port") {
    //    return instPort;
    if (if_name == "io_port") {
        return ioport;
    } else {
        // pass it along to our super class
        return MemObject::getSlavePort(if_name, idx);
    }
}

void
NegOffset::IOSidePort::sendPacket(PacketPtr pkt)
{
    // Note: This flow control is very simple since the memobj is blocking.

    panic_if(blockedPacket != nullptr, "Should never try to send if blocked!");
    //if(blockedPacket)
    //    return false;
    // If we can't send the packet across the port, store it for later.
    if (!sendTimingResp(pkt))// {
        blockedPacket = pkt;
    //} else
    //    owner->rubyport.trySendRetry();

    //return true;
}

AddrRangeList
NegOffset::IOSidePort::getAddrRanges() const
{
    return owner->getAddrRanges();
}

void
NegOffset::IOSidePort::trySendRetry()
{
    if (needRetry && blockedPacket == nullptr) {
        // Only send a retry if the port is now completely free
        needRetry = false;
        DPRINTF(NegOffset, "Sending retry req for %d\n", id);
        sendRetryReq();
    }
}

void
NegOffset::IOSidePort::recvFunctional(PacketPtr pkt)
{
    // Just forward to the memobj.
    return owner->handleFunctional(pkt);
}

bool
NegOffset::IOSidePort::recvTimingReq(PacketPtr pkt)
{
    // Just forward to the memobj.
    if (!owner->handleRequest(pkt)) {
        needRetry = true;
        return false;
    } else {
        return true;
    }
}

void
NegOffset::IOSidePort::recvRespRetry()
{
    // We should have a blocked packet if this function is called.
    assert(blockedPacket != nullptr);

    // Grab the blocked packet.
    PacketPtr pkt = blockedPacket;
    blockedPacket = nullptr;

    // Try to resend it. It's possible that it fails again.
    sendPacket(pkt);
    //owner->RubyPortRetryResp();
}

void
NegOffset::RubySidePort::sendPacket(PacketPtr pkt)
{
    // Note: This flow control is very simple since the memobj is blocking.

    panic_if(blockedPacket != nullptr, "Should never try to send if blocked!");
    //if(blockedPacket)
    //    return false;
    // If we can't send the packet across the port, store it for later.
    if (!sendTimingReq(pkt))// {
        blockedPacket = pkt;
    //} else
    //    owner->ioport.trySendRetry();
    //return true;
}

void
NegOffset::RubySidePort::trySendRetry()
{
    if (needRetry && blockedPacket == nullptr) {
        // Only send a retry if the port is now completely free
        needRetry = false;
        DPRINTF(NegOffset, "Sending retry req for %d\n", id);
        sendRetryResp();
    }
}

bool
NegOffset::RubySidePort::recvTimingResp(PacketPtr pkt)
{
    // Just forward to the memobj.
    return owner->handleResponse(pkt);
}

void
NegOffset::RubySidePort::recvReqRetry()
{
    // We should have a blocked packet if this function is called.
    assert(blockedPacket != nullptr);

    // Grab the blocked packet.
    PacketPtr pkt = blockedPacket;
    blockedPacket = nullptr;

    // Try to resend it. It's possible that it fails again.
    sendPacket(pkt);
    //owner->IOPortRetryReq();
}

void
NegOffset::RubySidePort::recvRangeChange()
{
    owner->sendRangeChange();
}

/*void
NegOffset::IOPortRetryReq()
{
    ioport.sendRetryReq();
}

void
NegOffset::RubyPortRetryResp()
{
    rubyport.sendRetryResp();
}*/

bool
NegOffset::handleRequest(PacketPtr pkt)
{
    assert(tileID != -1);
    if (blocked) {
        // There is currently an outstanding request. Stall.
        return false;
    }

    DPRINTF(NegOffset, "Got request for addr %#x\n", pkt->getAddr());

    // This memobj is now blocked waiting for the response to this packet.
    //blocked = true;

    //inform("neg pkt_addr:%x\n", pkt->getAddr());
    if(tileID == 1 && bits(pkt->getAddr(), 36, 36) == 0x1) {
        if (((pkt->getAddr() >> 60) == 0x2)
            || ((pkt->getAddr() >> 60) == 0xA)
            || ((pkt->getAddr() >> 60) == 0x8))
            pkt->setAddr(pkt->getAddr() - 0x1000000000);        
    }
    // Simply forward to the memory port
    blocked = true;
    rubyport.sendPacket(pkt);

    //rubyport.trySendRetry();
    return true;
}

bool
NegOffset::handleResponse(PacketPtr pkt)
{
    assert(blocked);
    DPRINTF(NegOffset, "Got response for addr %#x\n", pkt->getAddr());

    // The packet is now done. We're about to put it in the port, no need for
    // this object to continue to stall.
    // We need to free the resource before sending the packet in case the CPU
    // tries to send another request immediately (e.g., in the same callchain).
    blocked = false;

    if(tileID == 1 && bits(pkt->getAddr(), 36, 36) == 0x0) {
        if (((pkt->getAddr() >> 60) == 0x2) 
            || ((pkt->getAddr() >> 60) == 0xA)
            || ((pkt->getAddr() >> 60) == 0x8)) {
            pkt->setAddr(pkt->getAddr() + 0x1000000000);
            //inform("pkt_addr:%x\n", pkt->getAddr());
        }
    }
    // Simply forward to the memory port
    //if (pkt->req->isInstFetch()) {
    //    instPort.sendPacket(pkt);
    //} else {
    ioport.sendPacket(pkt);
    //}

    // For each of the cpu ports, if it needs to send a retry, it should do it
    // now since this memory object may be unblocked now.
    //instPort.trySendRetry();
    ioport.trySendRetry();

    return true;
}

void
NegOffset::handleFunctional(PacketPtr pkt)
{
    // Just pass this on to the memory side to handle for now.
    rubyport.sendFunctional(pkt);
}

AddrRangeList
NegOffset::getAddrRanges() const
{
    DPRINTF(NegOffset, "Sending new ranges\n");
    // Just use the same ranges as whatever is on the memory side.
    return rubyport.getAddrRanges();
}

void
NegOffset::sendRangeChange()
{
    //instPort.sendRangeChange();
    ioport.sendRangeChange();
}



NegOffset*
NegOffsetParams::create()
{
    return new NegOffset(this);
}
