/*
 * Copyright (c) 2017 Jason Lowe-Power
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Jason Lowe-Power
 */

#include "sys_conf/offset.hh"

#include "debug/Offset.hh"

Offset::Offset(OffsetParams *params) :
    MemObject(params),
    //instPort(params->name + ".inst_port", this),
    rubyport(params->name + ".ruby_port", this),
    ioport(params->name + ".io_port", this),
    tileID(params->tileID),
    blocked(false),
    Diskblocked(false)
{
}

BaseMasterPort&
Offset::getMasterPort(const std::string& if_name, PortID idx)
{
    panic_if(idx != InvalidPortID, "This object doesn't support vector ports");

    // This is the name from the Python SimObject declaration (Offset.py)
    if (if_name == "io_port") {
        return ioport;
    } else {
        // pass it along to our super class
        return MemObject::getMasterPort(if_name, idx);
    }
}

BaseSlavePort&
Offset::getSlavePort(const std::string& if_name, PortID idx)
{
    panic_if(idx != InvalidPortID, "This object doesn't support vector ports");

    // This is the name from the Python SimObject declaration in SimpleCache.py
    //if (if_name == "inst_port") {
    //    return instPort;
    if (if_name == "ruby_port") {
        return rubyport;
    } else {
        // pass it along to our super class
        return MemObject::getSlavePort(if_name, idx);
    }
}

void
Offset::RubySidePort::sendPacket(PacketPtr pkt)
{
    // Note: This flow control is very simple since the memobj is blocking.

    panic_if(blockedPacket != nullptr, "Should never try to send if blocked!");
    //if(blockedPacket)
    //    return false;
    // If we can't send the packet across the port, store it for later.
    if (!sendTimingResp(pkt))
        blockedPacket = pkt;
    //} else
    //    owner->ioport.trySendRetry();

    //return true;
}

AddrRangeList
Offset::RubySidePort::getAddrRanges() const
{
    return owner->getAddrRanges();
}

void
Offset::RubySidePort::trySendRetry()
{
    if (needRetry && blockedPacket == nullptr) {
        // Only send a retry if the port is now completely free
        needRetry = false;
        DPRINTF(Offset, "Sending retry req for %d\n", id);
        sendRetryReq();
    }
}

void
Offset::RubySidePort::recvFunctional(PacketPtr pkt)
{
    // Just forward to the memobj.
    return owner->handleFunctional(pkt);
}

bool
Offset::RubySidePort::recvTimingReq(PacketPtr pkt)
{
    // Just forward to the memobj.
    if (!owner->handleRequest(pkt)) {
        needRetry = true;
        return false;
    } else {
        return true;
    }
}

void
Offset::RubySidePort::recvRespRetry()
{
    // We should have a blocked packet if this function is called.
    assert(blockedPacket != nullptr);

    // Grab the blocked packet.
    PacketPtr pkt = blockedPacket;
    blockedPacket = nullptr;

    // Try to resend it. It's possible that it fails again.
    sendPacket(pkt);
    //    blockedPacket = pkt;
    //owner->IOPortRetryResp();
}

void
Offset::IOSidePort::sendPacket(PacketPtr pkt)
{
    // Note: This flow control is very simple since the memobj is blocking.

    panic_if(blockedPacket != nullptr, "Should never try to send if blocked!");
    // If we can't send the packet across the port, store it for later.
    //if(blockedPacket)
    //    return false;
    if (!sendTimingReq(pkt))
        blockedPacket = pkt;
    //} else
    //    owner->rubyport.trySendRetry();
    //return true;
}

void
Offset::IOSidePort::trySendRetry()
{
    if (needRetry && blockedPacket == nullptr) {
        // Only send a retry if the port is now completely free
        needRetry = false;
        DPRINTF(Offset, "Sending Resp for %d\n", id);
        sendRetryResp();
    }
}


bool
Offset::IOSidePort::recvTimingResp(PacketPtr pkt)
{
    // Just forward to the memobj.
    return owner->handleResponse(pkt);
    //if(!owner->handleResponse(pkt)) {
    //    needRetry = true;
    //    return false;
    //} else
    //    return true;
}

void
Offset::IOSidePort::recvReqRetry()
{
    // We should have a blocked packet if this function is called.
    assert(blockedPacket != nullptr);

    // Grab the blocked packet.
    PacketPtr pkt = blockedPacket;
    blockedPacket = nullptr;

    // Try to resend it. It's possible that it fails again.
    sendPacket(pkt);
    //    blockedPacket = pkt;
    //owner->RubyPortRetryReq();
}
/*
bool
Offset::IOSidePort::block(PacketPtr pkt)
{
    if(blockedPacket)
        return false;
    else {
        blockedPacket = pkt;
        return true;
    }
}

void
Offset::IOSidePort::sendblocked()
{
    if(blockedPacket) {
        PacketPtr pkt = blockedPacket;
        blockedPacket = nullptr;
        sendPacket(pkt);
    }
}
*/
void
Offset::IOSidePort::recvRangeChange()
{
    owner->sendRangeChange();
}

/*void
Offset::RubyPortRetryReq()
{
    rubyport.sendRetryReq();
}

void
Offset::IOPortRetryResp()
{
    ioport.sendRetryResp();
}*/

bool
Offset::handleRequest(PacketPtr pkt)
{
    if(blocked) {
        return false;
    }
    assert(tileID != -1);
    pkt->req->setTileId(tileID);
    DPRINTF(Offset, "Got request for addr %#x\n", pkt->getAddr());

    // This memobj is now blocked waiting for the response to this packet.
    //blocked = true;
    //uint8_t *pkt_data(pkt->getPtr<uint8_t>());
    if(tileID == 1 && bits(pkt->getAddr(), 36, 36) == 0x0) {
        if (((pkt->getAddr() >> 60) == 0x2)
            || ((pkt->getAddr() >> 60) == 0xA)
            || ((pkt->getAddr() >> 60) == 0x8)) {
            pkt->setAddr(pkt->getAddr() + 0x1000000000);
            //ioport.sendPacket(pkt);
            //if(!((pkt->getAddr() >> 60) == 0x8))
            //    inform("offset req_addr:%x\n", pkt->getAddr());
        } //else if(bits((pkt->getAddr() >> 8), 7, 3) == 4) {  //considering confDeviceBits = 0x8
          //  inform("offset req_addr:%x, tile:%d\n", pkt->getAddr(), tileID);
          //  std::fill(pkt_data, pkt_data + pkt->getSize(), 0xFF);
          //  pkt->makeAtomicResponse();
          //  inv_pcipkt = pkt;
          //  schedule(InvPciPktEvent, curTick() + 5);
        //}
    }
    if (Diskblocked) {
        // There is currently an outstanding request. Stall.
        for(const auto M5_VAR_USED &r : DiskRanges) {
            if(r.contains(pkt->getAddr()))
                return false;
        }
    } 
    //inform("%" PRIu64 ", Tile:%d, Got request for addr %#x\n", curTick(), tileID, pkt->getAddr());
      //  inform("offset req_addr:%x\n", pkt->getAddr());
    blocked = true;
    // Simply forward to the memory port
    ioport.sendPacket(pkt);

    //ioport.trySendRetry();
    return true;
}

bool
Offset::handleResponse(PacketPtr pkt)
{
    assert(blocked);
    DPRINTF(Offset, "Got response for addr %#x\n", pkt->getAddr());

    // The packet is now done. We're about to put it in the port, no need for
    // this object to continue to stall.
    // We need to free the resource before sending the packet in case the CPU
    // tries to send another request immediately (e.g., in the same callchain).
    blocked = false;

    if(tileID == 1 && bits(pkt->getAddr(), 36, 36) == 0x1) {
        if (((pkt->getAddr() >> 60) == 0x2)
            || ((pkt->getAddr() >> 60) == 0xA)
            || ((pkt->getAddr() >> 60) == 0x8))
            pkt->setAddr(pkt->getAddr() - 0x1000000000);
        //else if ((pkt->getAddr() >> 60) == 0xC) {
        //    if(pkt->getAddr() & 0x2000) == 0x2000)
                
    }
    // Simply forward to the memory port
    //if (pkt->req->isInstFetch()) {
    //    instPort.sendPacket(pkt);
    //} else {
    rubyport.sendPacket(pkt);
    //}

    // For each of the cpu ports, if it needs to send a retry, it should do it
    // now since this memory object may be unblocked now.
    //instPort.trySendRetry();
    rubyport.trySendRetry();

    return true;
}

void
Offset::handleFunctional(PacketPtr pkt)
{
    // Just pass this on to the memory side to handle for now.
    ioport.sendFunctional(pkt);
}

AddrRangeList
Offset::getAddrRanges() const
{
    DPRINTF(Offset, "Sending new ranges\n");
    // Just use the same ranges as whatever is on the memory side.
    return ioport.getAddrRanges();
}

void
Offset::sendRangeChange()
{
    //instPort.sendRangeChange();
    rubyport.sendRangeChange();
}

Offset*
OffsetParams::create()
{
    return new Offset(this);
}
