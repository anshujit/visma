/*
 * Copyright (c) 2017 Jason Lowe-Power
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Jason Lowe-Power
 */

#include "sys_conf/pcioffset.hh"

#include "debug/PciOffset.hh"

PciOffset::PciOffset(PciOffsetParams *params) :
    MemObject(params),
    //instPort(params->name + ".inst_port", this),
    busport(params->name + ".ruby_port", this),
    ioport(params->name + ".io_port", this),
    tileID(params->tileID),
    blocked(false)
    //InvPciPktEvent(this, false)
{
}

BaseMasterPort&
PciOffset::getMasterPort(const std::string& if_name, PortID idx)
{
    panic_if(idx != InvalidPortID, "This object doesn't support vector ports");

    // This is the name from the Python SimObject declaration (PciOffset.py)
    if (if_name == "io_port") {
        return ioport;
    } else {
        // pass it along to our super class
        return MemObject::getMasterPort(if_name, idx);
    }
}

BaseSlavePort&
PciOffset::getSlavePort(const std::string& if_name, PortID idx)
{
    panic_if(idx != InvalidPortID, "This object doesn't support vector ports");

    // This is the name from the Python SimObject declaration in SimpleCache.py
    //if (if_name == "inst_port") {
    //    return instPort;
    if (if_name == "bus_port") {
        return busport;
    } else {
        // pass it along to our super class
        return MemObject::getSlavePort(if_name, idx);
    }
}

void
PciOffset::BusSidePort::sendPacket(PacketPtr pkt)
{
    // Note: This flow control is very simple since the memobj is blocking.

    panic_if(blockedPacket != nullptr, "Should never try to send if blocked!");
    //if(blockedPacket)
    //    return false;
    // If we can't send the packet across the port, store it for later.
    if (!sendTimingResp(pkt))
        blockedPacket = pkt;
    //} else
    //    owner->ioport.trySendRetry();

    //return true;
}

AddrRangeList
PciOffset::BusSidePort::getAddrRanges() const
{
    return owner->getAddrRanges();
}

void
PciOffset::BusSidePort::trySendRetry()
{
    if (needRetry && blockedPacket == nullptr) {
        // Only send a retry if the port is now completely free
        needRetry = false;
        DPRINTF(PciOffset, "Sending retry req for %d\n", id);
        sendRetryReq();
    }
}

void
PciOffset::BusSidePort::recvFunctional(PacketPtr pkt)
{
    // Just forward to the memobj.
    return owner->handleFunctional(pkt);
}

bool
PciOffset::BusSidePort::recvTimingReq(PacketPtr pkt)
{
    // Just forward to the memobj.
    if (!owner->handleRequest(pkt)) {
        needRetry = true;
        return false;
    } else {
        return true;
    }
}

void
PciOffset::BusSidePort::recvRespRetry()
{
    // We should have a blocked packet if this function is called.
    assert(blockedPacket != nullptr);

    // Grab the blocked packet.
    PacketPtr pkt = blockedPacket;
    blockedPacket = nullptr;

    // Try to resend it. It's possible that it fails again.
    sendPacket(pkt);
}

void
PciOffset::IOSidePort::sendPacket(PacketPtr pkt)
{
    // Note: This flow control is very simple since the memobj is blocking.

    panic_if(blockedPacket != nullptr, "Should never try to send if blocked!");
    // If we can't send the packet across the port, store it for later.
    //if(blockedPacket)
    //    return false;
    if (!sendTimingReq(pkt))
        blockedPacket = pkt;
    //} else
    //    owner->busport.trySendRetry();
    //return true;
}

void
PciOffset::IOSidePort::trySendRetry()
{
    if (needRetry && blockedPacket == nullptr) {
        // Only send a retry if the port is now completely free
        needRetry = false;
        DPRINTF(PciOffset, "Sending Resp for %d\n", id);
        sendRetryResp();
    }
}

bool
PciOffset::IOSidePort::recvTimingResp(PacketPtr pkt)
{
    // Just forward to the memobj.
    return owner->handleResponse(pkt);
}

void
PciOffset::IOSidePort::recvReqRetry()
{
    // We should have a blocked packet if this function is called.
    assert(blockedPacket != nullptr);

    // Grab the blocked packet.
    PacketPtr pkt = blockedPacket;
    blockedPacket = nullptr;

    // Try to resend it. It's possible that it fails again.
    sendPacket(pkt);
}

void
PciOffset::IOSidePort::recvRangeChange()
{
    owner->sendRangeChange();
}

bool
PciOffset::handleRequest(PacketPtr pkt)
{
    if (blocked) {
        // There is currently an outstanding request. Stall.
        return false;
    }

    assert(tileID != -1);
    //pkt->req->setTileId(tileID);
    DPRINTF(PciOffset, "Got request for addr %#x\n", pkt->getAddr());
    //inform("Got request for addr %#x\n", pkt->getAddr());
    // This memobj is now blocked waiting for the response to this packet.
    //blocked = true;
    //uint8_t *pkt_data(pkt->getPtr<uint8_t>());
    if(tileID == 1 && bits(pkt->getAddr(), 36, 36) == 0x1) {  
        //if ((pkt->getAddr() >> 60) == 0x8) {
            pkt->setAddr(pkt->getAddr() - 0x1000000000);
            //ioport.sendPacket(pkt);
            //if(!((pkt->getAddr() >> 60) == 0x8))
            //    inform("offset req_addr:%x\n", pkt->getAddr());
        //} //else if(bits((pkt->getAddr() >> 8), 7, 3) == 4) {  //considering confDeviceBits = 0x8
          //  inform("offset req_addr:%x, tile:%d\n", pkt->getAddr(), tileID);
          //  std::fill(pkt_data, pkt_data + pkt->getSize(), 0xFF);
          //  pkt->makeAtomicResponse();
          //  inv_pcipkt = pkt;
          //  schedule(InvPciPktEvent, curTick() + 5);
        //}
    } //else if(tileID == 0) {
        //if((pkt->getAddr() >> 60 == 0xC) && (bits(pkt->getAddr() >> 8, 7, 3) == 8)) {      //considering confDeviceBits = 0x8
        //    std::fill(pkt_data, pkt_data + pkt->getSize(), 0xFF);
        //    pkt->makeAtomicResponse();
        //    inv_pcipkt = pkt;
        //    schedule(InvPciPktEvent, curTick() + 5);
        //} else
        //    ioport.sendPacket(pkt);
    //}
    //inform("%" PRIu64 ", Tile:%d, Got request for addr %#x\n", curTick(), tileID, pkt->getAddr());
    blocked = true;
    // Simply forward to the memory port
    ioport.sendPacket(pkt);

    return true;
}

bool
PciOffset::handleResponse(PacketPtr pkt)
{
    assert(blocked);
    DPRINTF(PciOffset, "Got response for addr %#x\n", pkt->getAddr());
    //inform("Got response for addr %#x\n", pkt->getAddr());
    // The packet is now done. We're about to put it in the port, no need for
    // this object to continue to stall.
    // We need to free the resource before sending the packet in case the CPU
    // tries to send another request immediately (e.g., in the same callchain).
    blocked = false;

    if(tileID == 1 && bits(pkt->getAddr(), 36, 36) == 0x0) {
    //    if ((pkt->getAddr() >> 60) == 0x8)
            pkt->setAddr(pkt->getAddr() + 0x1000000000);
        //else if ((pkt->getAddr() >> 60) == 0xC) {
        //    if(pkt->getAddr() & 0x2000) == 0x2000)
                
    }
    // Simply forward to the memory port
    //if (pkt->req->isInstFetch()) {
    //    instPort.sendPacket(pkt);
    //} else {
    busport.sendPacket(pkt);
    //}

    // For each of the cpu ports, if it needs to send a retry, it should do it
    // now since this memory object may be unblocked now.
    //instPort.trySendRetry();
    busport.trySendRetry();

    return true;
}

void
PciOffset::handleFunctional(PacketPtr pkt)
{
    // Just pass this on to the memory side to handle for now.
    ioport.sendFunctional(pkt);
}

AddrRangeList
PciOffset::getAddrRanges() const
{
    DPRINTF(PciOffset, "Sending new ranges\n");
    // Just use the same ranges as whatever is on the memory side.
    AddrRangeList ranges = ioport.getAddrRanges();
    if(tileID == 1) {
        AddrRangeList ranges_tile; 
        for (const auto M5_VAR_USED &r : ranges) {
            AddrRange range(r.start() + 0x1000000000, r.end() + 0x1000000000);
            ranges_tile.push_back(range);
            //inform("%s\n", range.to_string());
        }
        //for(int i = 0; i < offsets.size(); i++)
        //    offsets[i]->setDiskAddrRange(ranges_tile);

        return ranges_tile;
    }
    //for(const auto M5_VAR_USED &r : ranges)
    //    inform("%s\n", r.to_string()); 
    //for(int i = 0; i < offsets.size(); i++)
    //    offsets[i]->setDiskAddrRange(ranges);
    return ioport.getAddrRanges();
}

void
PciOffset::sendRangeChange()
{
    //instPort.sendRangeChange();
    busport.sendRangeChange();
}

//void
//PciOffset::ProcessInvPciPktEvent()
//{
//    busport.sendPacket(inv_pcipkt);
//    inv_pcipkt = nullptr;
//    blocked = false;
//    //schedule(InvPciPktEvent, curTick() + 5);
//}

PciOffset*
PciOffsetParams::create()
{
    return new PciOffset(this);
}
