#include <vector>
#include <iostream>
#include "sys_conf/tile.hh"
#include "base/intmath.hh"

using namespace std;
tile::tile(std::vector<connBits *> conns, std::vector<LUT *> luts, int id)
{
	m_conns = conns;
	m_luts = luts;
	tile_id = id;
}

int
tile::createTile(int num_cores, Matrix routers)
{
	if(num_cores == 4) {
		//For Square-Shape type
		for(int i = 0; i < routers.size() - 1; i++) {
			int count = -1;
			for(int j = 0; j < routers[i].size() - 1; j++) {
				if(routers[i][j] == 1 && routers[i][j + 1] == 1) {
					if(routers[i + 1][j] == 1 && routers[i + 1][j + 1] == 1) {
						router_ids.push_back(i * routers[i].size() + j);
						count++;
						router_ids.push_back(i * routers[i].size() + (j + 1));
						count++;							
						
						router_ids.push_back((i + 1) * routers[i].size() + j);
						count++;
						router_ids.push_back((i + 1) * routers[i].size() + (j + 1));
						count++;
					}
					if(count == 3) {
						type = "Square-Shape";
						return 1;
					}
				}
			}

			//For T-Shape type
			for(int j = 0; j < routers[i].size() - 2; j++) {
				if(routers[i][j] == 1 && routers[i][j + 1] == 1 && routers[i][j + 2] == 1) {
					if(routers[i + 1][j + 1] == 1) {
						router_ids.push_back(i * routers[i].size() + j);
						count++;
						router_ids.push_back(i * routers[i].size() + (j + 1));
						count++;							
						router_ids.push_back(i * routers[i].size() + (j + 2));
						count++;						

						router_ids.push_back((i + 1) * routers[i].size() + (j + 1));
						count++;
					}
					if(count == 3) {
						type = "T-Shape";
						return 1;
					}
				}
			}

			//For Zig-Zag type
			for(int j = 0; j < routers[i].size() - 2; j++) {
				if(routers[i][j] == 1 && routers[i][j + 1] == 1) {
					if(routers[i + 1][j + 1] == 1 && routers[i + 1][j + 2] == 1) {
						router_ids.push_back(i * routers[i].size() + j);
						count++;
						router_ids.push_back(i * routers[i].size() + (j + 1));
						count++;							
						
						router_ids.push_back((i + 1) * routers[i].size() + (j + 1));
						count++;
						router_ids.push_back((i + 1) * routers[i].size() + (j + 2));
						count++;
					}
					if(count == 3) {
						type = "Zig-Zag";
						return 1;
					}
				}
			}
		}
        return -2;		
	}

	else if(num_cores == 3) {
		//For L-Shape type
		for(int i = 0; i < routers.size() - 1; i++) {
			int count = -1;
			for(int j = 0; j < routers[i].size() - 1; j++) {
				if(routers[i][j] == 1) {
					if(routers[i + 1][j] == 1 && routers[i + 1][j + 1] == 1) {
						router_ids.push_back(i * routers[i].size() + j);
						count++;						
				
						router_ids.push_back((i + 1) * routers[i].size() + j);
						count++;
						router_ids.push_back((i + 1) * routers[i].size() + (j + 1));
						count++;
					}
					if(count == 2) {
						type = "L-Shape";
						return 1;
					}
				}
			}
		}
        return -2;
	}

    else
        return -1;
}

bool
tile::findlink(char x, std::vector<char> links)
{
	for(int i = 0; i < links.size(); i++) {
		if(links[i] == x)
			return true;
	}
	return false;
}
			
void
tile::SqTileconnBits(int local_id, int global_id, std::vector<char> links)
{
	int i;

	//resetconnBit is to assign 0
	//setconnBit is to assign 1
		if(local_id == 0) {
			i = 2;
			if(findlink('N', links)) {
				m_conns[global_id]->resetconnBit(i);
				i++;
			}
			if(findlink('W', links)) {
				m_conns[global_id]->resetconnBit(i);
				i++;
			}
			m_conns[global_id]->setconnBit(i);
			i++;
			m_conns[global_id]->setconnBit(i);
		}
		else if(local_id == 1) {
			i = 2;
			if(findlink('N', links)) {
				m_conns[global_id]->resetconnBit(i);
				i++;
			}
			m_conns[global_id]->setconnBit(i);
			i++;
			if(findlink('E', links)) {
				m_conns[global_id]->resetconnBit(i);
				i++;
			}
			m_conns[global_id]->setconnBit(i);
		}
		else if(local_id == 2) {
			i = 2;
			m_conns[global_id]->setconnBit(i);
			i++;
			if(findlink('W', links)) {
				m_conns[global_id]->resetconnBit(i);
				i++;
			}
			m_conns[global_id]->setconnBit(i);
			i++;
			if(findlink('S', links))
				m_conns[global_id]->resetconnBit(i);
		}
		else {
			i = 2;
			m_conns[global_id]->setconnBit(i);
			i++;
			m_conns[global_id]->setconnBit(i);
			i++;
			if(findlink('E', links)) {
				m_conns[global_id]->resetconnBit(i);
				i++;
			}
			if(findlink('S', links))
				m_conns[global_id]->resetconnBit(i);
		}
}

void
tile::TTileconnBits(int local_id, int global_id, std::vector<char> links)
{

	int i;
		if(local_id == 0) {
			i = 2;
			if(findlink('N', links)) {
				m_conns[global_id]->resetconnBit(i);
				i++;
			}
			if(findlink('W', links)) {
				m_conns[global_id]->resetconnBit(i);
				i++;
			}
			m_conns[global_id]->setconnBit(i);
			i++;
			m_conns[global_id]->resetconnBit(i);
		}

		else if(local_id == 1) {
			i = 2;
			if(findlink('N', links)) {
				m_conns[global_id]->resetconnBit(i);
				i++;
			}
			m_conns[global_id]->setconnBit(i);
			i++;
			m_conns[global_id]->setconnBit(i);
			i++;			
			m_conns[global_id]->setconnBit(i);
		}

		else if(local_id == 2) {
			i = 2;
			if(findlink('N', links)) {
				m_conns[global_id]->resetconnBit(i);
				i++;
			}
			m_conns[global_id]->setconnBit(i);
			i++;
			if(findlink('E', links)) {
				m_conns[global_id]->resetconnBit(i);
				i++;
			}
			m_conns[global_id]->resetconnBit(i);
		}

		else {
			i = 2;
			m_conns[global_id]->setconnBit(i);
			i++;
			m_conns[global_id]->resetconnBit(i);
			i++;
			m_conns[global_id]->resetconnBit(i);
			i++;
			if(findlink('S', links))
				m_conns[global_id]->resetconnBit(i);
		}
}

void
tile::ZigZagTileconnBits(int local_id, int global_id, std::vector<char> links)
{

	int i;
		if(local_id == 0) {
			i = 2;
			if(findlink('N', links)) {
				m_conns[global_id]->resetconnBit(i);
				i++;
			}
			if(findlink('W', links)) {
				m_conns[global_id]->resetconnBit(i);
				i++;
			}
			m_conns[global_id]->setconnBit(i);
			i++;
			m_conns[global_id]->resetconnBit(i);
		}

		else if(local_id == 1) {
			i = 2;
			if(findlink('N', links)) {
				m_conns[global_id]->resetconnBit(i);
				i++;
			}
			m_conns[global_id]->setconnBit(i);
			i++;
			m_conns[global_id]->resetconnBit(i);
			i++;
			m_conns[global_id]->setconnBit(i);
		}

		else if(local_id == 2) {
			i = 2;
			m_conns[global_id]->setconnBit(i);
			i++;
			m_conns[global_id]->resetconnBit(i);
			i++;
			m_conns[global_id]->setconnBit(i);
			i++;
			if(findlink('S', links))
				m_conns[global_id]->resetconnBit(i);
		}

		else {
			i = 2;
			m_conns[global_id]->setconnBit(i);
			i++;
			m_conns[global_id]->resetconnBit(i);
			i++;
			if(findlink('E', links)) {
				m_conns[global_id]->resetconnBit(i);
				i++;
			}
			if(findlink('S', links))
				m_conns[global_id]->resetconnBit(i);
		}	
}

void
tile::LTileconnBits(int local_id, int global_id, std::vector<char> links)
{
	int i;
		if(local_id == 0) {
			i = 2;
			if(findlink('N', links)) {
				m_conns[global_id]->resetconnBit(i);
				i++;
			}
			if(findlink('W', links)) {
				m_conns[global_id]->resetconnBit(i);
				i++;
			}
			m_conns[global_id]->resetconnBit(i);
			i++;
			m_conns[global_id]->setconnBit(i);
		}

		else if(local_id == 1) {
			i = 2;
			if(findlink('N', links)) {
				m_conns[global_id]->setconnBit(i);
				i++;
			}
			if(findlink('W', links)) {
				m_conns[global_id]->resetconnBit(i);
				i++;
			}
			m_conns[global_id]->setconnBit(i);
            i++;
			if(findlink('S', links))
				m_conns[global_id]->resetconnBit(i);
		}

		else {
			i = 2;
			m_conns[global_id]->resetconnBit(i);
			i++;
			m_conns[global_id]->setconnBit(i);
			i++;
			if(findlink('E', links)) {
				m_conns[global_id]->resetconnBit(i);
				i++;
			}
			if(findlink('S', links))
				m_conns[global_id]->resetconnBit(i);
		}	
}

void
tile::nbrconnBits(std::vector<int> nbr_ids, int global_id, std::vector<std::vector<char>> allinks)
{
	int flag;
	int x;
	for(int i = 0; i < nbr_ids.size(); i++) {
		flag = 0;
		for(int j = 0; j < router_ids.size(); j++) {
			if(nbr_ids[i] == router_ids[j]) {
				flag = 0;
				break;
			}
			else
				flag = 1;
		}

		if(flag == 1) {
			if(allinks[global_id][i] == 'N') {			//here there is a one-to-one mapping of neighbour ids and links
				x = 2;									//The value in "i" of nbr_ids corresponds to that of the links value 
				if(findlink('N', allinks[nbr_ids[i]]))
					x++;
				if(findlink('W', allinks[nbr_ids[i]]))
					x++;
				if(findlink('E', allinks[nbr_ids[i]]))
					x++;
				if(findlink('S', allinks[nbr_ids[i]]))
					m_conns[nbr_ids[i]]->resetconnBit(x);
			}

			if(allinks[global_id][i] == 'W') {
				x = 2;
				if(findlink('N', allinks[nbr_ids[i]]))
					x++;
				if(findlink('W', allinks[nbr_ids[i]]))
					x++;
				if(findlink('E', allinks[nbr_ids[i]]))
					m_conns[nbr_ids[i]]->resetconnBit(x);
			}
			if(allinks[global_id][i] == 'E') {
				x = 2;
				if(findlink('N', allinks[nbr_ids[i]]))
					x++;
				if(findlink('W', allinks[nbr_ids[i]]))
					m_conns[nbr_ids[i]]->resetconnBit(x);
			}		
			if(allinks[global_id][i] == 'S') {
				x = 2;
				if(findlink('N', allinks[nbr_ids[i]]))
					m_conns[nbr_ids[i]]->resetconnBit(x);
			}
		}
	}
}

bool
tile::findrouter(int id)
{
	for(int i = 0; i < router_ids.size(); i++) {
		if(id == router_ids[i])
			return true;
	}
	return false;
}

void
tile::tilelutBits(int dmas, int num_routers)
{
	int flag;
	for(int i = 0; i < m_luts.size() - dmas; i++) {
		flag = 0;
		for(int j = 0; j < router_ids.size(); j++) {
			if(i == router_ids[j] || (i % num_routers) == router_ids[j]) {    //LUTs within the tile
				if(local_luts.size() < 2 * router_ids.size())				
					local_luts.push_back(i);
				for(int k = 0; k < num_routers; k++) {
					if(findrouter(k))
						m_luts[i]->setlutBits(k);                             //nodes within the tile are set
					else
						m_luts[i]->resetlutBits(k);                           //nodes outside the tile are reset
				}
				flag = 1;                                                     //flag is set to indicate that this LUT
				break;                                                        //is local to (lies within) this tile
			}
		}

		if(flag == 0) {                                                       //LUTs lying outside of the tile
			for(int k = 0; k < num_routers; k++) {
				if(findrouter(k))
					m_luts[i]->resetlutBits(k);                               //reset the nodes that lie within this tile
			}
		}	
	}

    for(int i = 0; i < local_luts.size(); i++)
        m_luts[local_luts[i]]->tileid(tile_id);
}

void
tile::setBaseBoundAddr(int numa_high_bit, int num_dirs)
{
    BaseAddrPtr = (uint64_t)router_ids.front() << (numa_high_bit - ceilLog2(num_dirs) + 1);
    BoundAddrPtr = (uint64_t)(router_ids.back() + 1) << (numa_high_bit - ceilLog2(num_dirs) + 1);
    BoundAddrPtr--;
    mask = 1 << (numa_high_bit + 1);
    mask--;
    pagePtr = BaseAddrPtr >> PageShift;
}

void
tile::setMidInvalidRange(int numa_high_bit, int num_dirs)
{
    int j;
    for(int i = 0; i < router_ids.size() - 1; i++) {
        j = i + 1;
        int diff = router_ids[j] - router_ids[i];
        if(diff != 1) {
            Addr invalidstart = (uint64_t) (router_ids[i] + 1) << (numa_high_bit - ceilLog2(num_dirs) + 1);
            Addr invalidend = (uint64_t) router_ids[j] << (numa_high_bit - ceilLog2(num_dirs) + 1);
            invalidend--;
            invalidranges.push_back(AddrRange(invalidstart, invalidend));
        }
    }
}

void
tile::registerThreadContext(ThreadContext *tc)
{
    threadcontexts.push_back(tc);
}

Addr
tile::allocPhysPages(int npages)
{
    Addr return_addr = pagePtr << PageShift;
    pagePtr += npages;
    Addr next_return_addr = pagePtr << PageShift;

    for(int i = 0; i < invalidranges.size(); i++) {
        if(invalidranges[i].contains(next_return_addr)) {
            return_addr = invalidranges[i].end() + 1;
            pagePtr = return_addr >> PageShift;
            pagePtr += npages;
            next_return_addr = pagePtr << PageShift;
        }
    }

    if(next_return_addr >= BoundAddrPtr)
        fatal("Tile memory is exhausted. Allocate more memory!!!");

    AddrRange m5opRange(0xffff0000, 0xffffffff);
    if (m5opRange.contains(next_return_addr)) {
        warn("Reached m5ops MMIO region\n");
        return_addr = 0xffffffff;
        pagePtr = 0xffffffff >> PageShift;
    }
    /*
    if ((pagePtr << PageShift) > physmem.totalSize())
        fatal("Out of memory, please increase size of physical memory.");*/
    return return_addr;
}

void
tile::print(std::ostream& out) const
{
	out << "Tile:" << std::dec << tile_id << std::endl;
	out << "Core ids:";
	for(int i = 0; i < router_ids.size(); i++)
		out << router_ids[i] << " ";
	out << std::endl;
	out << "LUTs:";
	for(int i = 0; i < local_luts.size(); i++)
		out << local_luts[i] << " ";
	out << std::endl;
    out << "Base addr:0x" << std::hex << BaseAddrPtr << std::endl;
    out << "Bound addr:0x" << BoundAddrPtr << std::endl;
    out << "Invalidrange:0x" << invalidranges.front().start() << ", 0x" << invalidranges.front().end() << std::endl;
}
