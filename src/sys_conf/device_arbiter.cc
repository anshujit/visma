/*
 * Copyright (c) 2017 Jason Lowe-Power
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met: redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer;
 * redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution;
 * neither the name of the copyright holders nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Jason Lowe-Power
 */

#include "sys_conf/device_arbiter.hh"

#include "debug/DeviceArbiter.hh"

DeviceArbiter::DeviceArbiter(DeviceArbiterParams *params) :
    MemObject(params),
    //instPort(params->name + ".inst_port", this),
    recv_req_quantum(params->recv_req_quantum),
    tx_req_quantum(params->tx_req_quantum),
    recv_resp_quantum(params->recv_resp_quantum),
    send_resp_quantum(params->send_resp_quantum),
    tx_latency(params->latency),
    throttlers(params->throttlers),
    //max_fifo_size(params->max_fifo_size),
    needRespRetry(false),
    dataPort(params->name + ".data_port", this),
    memPort(params->name + ".mem_side", this),
    blocked(false),
    //setTileIdEvent(this, false),
    stateChangeEvent(this, false),
    txEvent(this, false),
    rxEvent(this, false)
{
    //schedule(setTileIdEvent, curTick() + 34000000);
    //throttlers = params->throttlers;
    state = idle;
    //for(int i = 0; i < params->throttlers.size(); i++) {
    //  Throttler *throttler = safe_cast<Throttler*> (params->throttlers[i]);
    //    throttlers.push_back(throttler);
    //}
}

BaseMasterPort&
DeviceArbiter::getMasterPort(const std::string& if_name, PortID idx)
{
    panic_if(idx != InvalidPortID, "This object doesn't support vector ports");

    // This is the name from the Python SimObject declaration (SimpleMemobj.py)
    if (if_name == "mem_side") {
        return memPort;
    } else {
        // pass it along to our super class
        return MemObject::getMasterPort(if_name, idx);
    }
}

BaseSlavePort&
DeviceArbiter::getSlavePort(const std::string& if_name, PortID idx)
{
    panic_if(idx != InvalidPortID, "This object doesn't support vector ports");

    // This is the name from the Python SimObject declaration in SimpleCache.py
    //if (if_name == "inst_port") {
    //    return instPort;
    if (if_name == "data_port") {
        return dataPort;
    } else {
        // pass it along to our super class
        return MemObject::getSlavePort(if_name, idx);
    }
}

void
DeviceArbiter::CPUSidePort::sendPacket(PacketPtr pkt)
{
    // Note: This flow control is very simple since the memobj is blocking.

    panic_if(blockedPacket != nullptr, "Should never try to send if blocked!");

    // If we can't send the packet across the port, store it for later.
    if (!sendTimingResp(pkt)) {
        blockedPacket = pkt;
    }
}

AddrRangeList
DeviceArbiter::CPUSidePort::getAddrRanges() const
{
    return owner->getAddrRanges();
}

void
DeviceArbiter::CPUSidePort::trySendRetry()
{
    if (needRetry) {// && blockedPacket == nullptr) {
        // Only send a retry if the port is now completely free
        needRetry = false;
        DPRINTF(DeviceArbiter, "Sending retry req for %d\n", id);
        sendRetryReq();
    }
}

void
DeviceArbiter::CPUSidePort::recvFunctional(PacketPtr pkt)
{
    // Just forward to the memobj.
    return owner->handleFunctional(pkt);
}

bool
DeviceArbiter::CPUSidePort::recvTimingReq(PacketPtr pkt)
{
    // Just forward to the memobj.
    if (!owner->handleRequest(pkt)) {
        needRetry = true;
        return false;
    } else {
        return true;
    }
}

void
DeviceArbiter::CPUSidePort::recvRespRetry()
{
    // We should have a blocked packet if this function is called.
    //assert(blockedPacket != nullptr);

    // Grab the blocked packet.
    //PacketPtr pkt = blockedPacket;
    //blockedPacket = nullptr;

    //Try to resend it. It's possible that it fails again.
    //sendPacket(pkt);
}

void
DeviceArbiter::MemSidePort::sendPacket(PacketPtr pkt)
{
    // Note: This flow control is very simple since the memobj is blocking.

    panic_if(blockedPacket != nullptr, "Should never try to send if blocked!");

    // If we can't send the packet across the port, store it for later.
    if (!sendTimingReq(pkt)) {
        blockedPacket = pkt;
    }
}

bool
DeviceArbiter::MemSidePort::recvTimingResp(PacketPtr pkt)
{
    // Just forward to the memobj.
    return owner->handleResponse(pkt);
}

void
DeviceArbiter::MemSidePort::recvReqRetry()
{
    // We should have a blocked packet if this function is called.
    // assert(blockedPacket != nullptr);

    // Grab the blocked packet.
    //PacketPtr pkt = tx_fifos;
    //blockedPacket = nullptr;

    // Try to resend it. It's possible that it fails again.
    //sendPacket(pkt);
}

void
DeviceArbiter::MemSidePort::recvRangeChange()
{
    owner->sendRangeChange();
}
/*
void
DeviceArbiter::setTileId()
{
    //RRTileId();
    if(curr_id != -1) {
        _tileId = valid_ids[curr_id];
        state = recvReq;
        dataPort.trySendRetry();
    }

    schedule(stateChangeEvent, curTick() + quantum);
    //schedule(stateChangeEvent, curTick() + 2 * quantum);//clockEdge(Cycles(0 + quantum)));
    //schedule(stateChangeEvent, curTick() + 3 * quantum);
    schedule(setTileIdEvent, curTick() + 4 * quantum);
    //reschedule(txEvent, curTick());
}*/

void
DeviceArbiter::setTxTileId()
{
    if(tx_curr_id != -1) {
        tx_tileId = valid_ids[tx_curr_id];
        //state = recvReq;
        //dataPort.trySendRetry();
    }
}

void
DeviceArbiter::setRxTileId()
{
    if(rx_curr_id != -1) {
        rx_tileId = valid_ids[rx_curr_id];
        //state = recvReq;
        //dataPort.trySendRetry();
    }
}

/*
void
DeviceArbiter::RRTileId()
{
    if(curr_id < valid_ids.size() - 1)
        ++curr_id;
    else if(valid_ids.size() != 0)
        curr_id = 0;
    else
        curr_id = -1;
}
*/
void
DeviceArbiter::RRTxTileId()
{
    if(tx_curr_id < valid_ids.size() - 1)
        ++tx_curr_id;
    else if(valid_ids.size() != 0)
        tx_curr_id = 0;
    else
        tx_curr_id = -1;
}

void
DeviceArbiter::RRRxTileId()
{
    if(rx_curr_id < valid_ids.size() - 1)
        ++rx_curr_id;
    else if(valid_ids.size() != 0)
        rx_curr_id = 0;
    else
        rx_curr_id = -1;
}

void
DeviceArbiter::make_valid(int TileId, int fifo_size)
{
    valid_ids.push_back(TileId);
    std::deque<PacketPtr> tx_fifo;
    std::deque<PacketPtr> rx_fifo;
    //tx_fifos.push_back(std::make_pair(TileId, tx_fifo));
    //rx_fifos.push_back(std::make_pair(TileId, rx_fifo));
    tx_fifos[TileId] = tx_fifo;
    rx_fifos[TileId] = rx_fifo;
    max_fifo_size[TileId] = fifo_size;
    //deviceContexts.push_back(std::make_pair(TileId, AddrRange(start, end)));
}

//void
//DeviceArbiter::setThrottlers(std::vector<Throttler*> pkt_throttlers)
//{
//    throttlers = pkt_throttlers;
//}

void
DeviceArbiter::stateChange()
{
    switch(state) {
        case recvReq: //for(int i = 0; i < throttlers.size(); ++i) {
                      //    throttlers[i]->block_pkts();
                      //}
                      state = tx_req;
                      for(std::map<int, std::vector<int>>::iterator x=tile_throttlers.begin(); x!=tile_throttlers.end(); ++x)                   {
                          for(int i = 0; i < x->second.size(); ++i)
                             throttlers[x->second[i]]->block_pkts();
                      } 
                      
                      //for(int i = 0; i < valid_ids.size(); i++) {
                      //    RRTxTileId();
                          //if(!tx_fifos[valid_ids[tx_curr_id]].empty()) {
                          //    setTxTileId();
                          //state = tx_req;
                      RRTxTileId();
                      setTxTileId();
                      schedule(txEvent, curTick());
                      schedule(stateChangeEvent, curTick() + tx_req_quantum);
                      //state = tx_req;
                      //schedule(txEvent, curTick());
                      //if(state != tx_req) {
                      //    state = recvResp;
                      //    schedule(stateChangeEvent, curTick() + quantum);
                      //    if(needRespRetry)
                      //        memPort.sendRetryResp();
                      //}
                      break;
        case tx_req:  state = recvResp;
                      if(needRespRetry)
                          memPort.sendRetryResp();
                      schedule(stateChangeEvent, curTick() + recv_resp_quantum);
                      break;
        case recvResp://for(int i = 0; i < valid_ids.size(); i++) {
                      //    RRRxTileId();
                      //    if(!rx_fifos[valid_ids[rx_curr_id]].empty()) {
                      //        setRxTileId();

                      state = rx_resp;
                      RRRxTileId();
                      setRxTileId();
                      schedule(rxEvent, curTick());
                      schedule(stateChangeEvent, curTick() + send_resp_quantum);
                      
                      //state = rx_resp;
                      //schedule(rxEvent, curTick());
                      //if(state != rx_resp) {
                      //    state = recvReq;
                      //    schedule(stateChangeEvent, curTick() + quantum);
                      //    dataPort.trySendRetry();
                      //}
                      break;
        case rx_resp: state = recvReq;     //state change should be done first, otherwise problem occurs :)
                      for(std::map<int, std::vector<int>>::iterator x=tile_throttlers.begin(); x!=tile_throttlers.end(); ++x)                   {
                          if(tx_fifos[x->first].size() < max_fifo_size[x->first]) {
                              for(int i = 0; i < x->second.size(); ++i) {
                                  int version = throttlers[x->second[i]]->get_version();
                                  assert(version >= 0 && version < x->second.size());
                                  if(version > 0 && version < x->second.size())
                                      --version;
                                  else
                                      version = x->second.size() - 1;
                                  throttlers[x->second[i]]->unblock_pkts(version);
                              }
                          }
                      }
                      
                      schedule(stateChangeEvent, curTick() + recv_req_quantum);
                      //dataPort.trySendRetry();
                      break;
        case idle:    state = recvReq;
                      schedule(stateChangeEvent, curTick() + recv_req_quantum);
                      break;
    }
}

void
DeviceArbiter::transmitReq()
{
    assert(state == tx_req);
    //assert(!tx_fifos[tx_tileId].empty());
    //if(!tx_fifos[_tileId].second.empty()) {
        //if(memPort.sendTimingReq(tx_fifos[_tileId].second.front())) {
        //    tx_fifos[_tileId].second.pop_front();
    if(!tx_fifos[tx_tileId].empty()) {
        if(memPort.sendTimingReq(tx_fifos[tx_tileId].front())) {
            tx_fifos[tx_tileId].pop_front();
        }
        schedule(txEvent, curTick() + tx_latency);
    } else if(tx_tileId != valid_ids.back()) {
        RRTxTileId();
        setTxTileId();
        schedule(txEvent, curTick());
    }
}

void
DeviceArbiter::returnResp()
{
    assert(state == rx_resp);
    //assert(!rx_fifos[rx_tileId].empty());
    //if(!rx_fifos[_tileId].second.empty()) {
    //    if(dataPort.sendTimingResp(rx_fifos[_tileId].second.front())) {
    //        rx_fifos[_tileId].second.pop_front();
    if(!rx_fifos[rx_tileId].empty()) {
        if(dataPort.sendTimingResp(rx_fifos[rx_tileId].front())) {
            rx_fifos[rx_tileId].pop_front();
        }
        schedule(rxEvent, curTick() + tx_latency);
    } else if(rx_tileId != valid_ids.back()) {
        RRRxTileId();
        setRxTileId();
        schedule(rxEvent, curTick());
    }
}

bool
DeviceArbiter::handleRequest(PacketPtr pkt)
{
    if (state == idle)
        stateChange();
    else if (state != recvReq) {
        DPRINTF(DeviceArbiter, "Got blocked request while !recvReq from tile:%d\n", pkt->req->_tileId);
        return false;
    }

    //else if(tx_fifos[pkt->req->_tileId].second.size() < max_fifo_size)
    //    tx_fifos[pkt->req->_tileId].second.push_back(pkt);
    if(tx_fifos[pkt->req->_tileId].size() == max_fifo_size[pkt->req->_tileId]) {
        DPRINTF(DeviceArbiter, "Got blocked request after fifo full from tile:%d\n", pkt->req->_tileId);
        return false;
    }

    else if(tx_fifos[pkt->req->_tileId].size() < max_fifo_size[pkt->req->_tileId]) {
        tx_fifos[pkt->req->_tileId].push_back(pkt);
        if(tx_fifos[pkt->req->_tileId].size() == max_fifo_size[pkt->req->_tileId]) {
            for(int i = 0; i < tile_throttlers[pkt->req->_tileId].size(); ++i) {
                DPRINTF(DeviceArbiter, "Blocking thrott:%d in tile:%d\n", tile_throttlers[pkt->req->_tileId][i], pkt->req->_tileId);
                throttlers[tile_throttlers[pkt->req->_tileId][i]]->block_pkts();
            }
        }
    }
        

    //if(tx_fifos[pkt->req->_tileId].size() == max_fifo_size[pkt->req->_tileId]) {
    //    for(int i = 0; i < tile_throttlers[pkt->req->_tileId].size(); ++i) {
    //        throttlers[tile_throttlers[pkt->req->_tileId][i]]->block_pkts();
    //    }
    //}

    DPRINTF(DeviceArbiter, "Got request for addr %#x from tile:%d\n", pkt->getAddr(), pkt->req->_tileId);

    // This memobj is now blocked waiting for the response to this packet.
    //blocked = true;

    // Simply forward to the memory port
    // memPort.sendPacket(pkt);

    return true;
}

bool
DeviceArbiter::handleResponse(PacketPtr pkt)
{
    if(state != recvResp) {
        needRespRetry = true;
        return false;
    }
        
    //else if(rx_fifos[pkt->req->_tileId].second.size() < max_fifo_size) {
    //    DPRINTF(DeviceArbiter, "Got response for addr %#x to Tile:%d\n", pkt->getAddr(), pkt->req->_tileId);
    //    rx_fifos[pkt->req->_tileId].second.push_back(pkt);
    //    needRespRetry = false;
    //}
    else if(rx_fifos[pkt->req->_tileId].size() < max_fifo_size[pkt->req->_tileId]) {
        DPRINTF(DeviceArbiter, "Got response for addr %#x to Tile:%d\n", pkt->getAddr(), pkt->req->_tileId);
        rx_fifos[pkt->req->_tileId].push_back(pkt);
        needRespRetry = false;
    }
    else {
        needRespRetry = true;
        return false;
    }
    //if(pkt->req->_tileId != _tileId)
    //    return false;
    // The packet is now done. We're about to put it in the port, no need for
    // this object to continue to stall.
    // We need to free the resource before sending the packet in case the CPU
    // tries to send another request immediately (e.g., in the same callchain).
    //blocked = false;

    // Simply forward to the memory port
    //if (pkt->req->isInstFetch()) {
    //    instPort.sendPacket(pkt);
    //} else {
    //    dataPort.sendPacket(pkt);
    //}

    // For each of the cpu ports, if it needs to send a retry, it should do it
    // now since this memory object may be unblocked now.
    //instPort.trySendRetry();
    //dataPort.trySendRetry();

    return true;
}

void
DeviceArbiter::setTileThrottlers(int tileId, std::vector<int> throttler_ids)
{
    tile_throttlers[tileId] = throttler_ids;
    for(int i = 0; i < tile_throttlers[tileId].size(); ++i)
        throttlers[tile_throttlers[tileId][i]]->set_version(i);      
}

void
DeviceArbiter::setnewfifosize(int tileId, int size)
{
    max_fifo_size[tileId] = size;
}

void
DeviceArbiter::remove_fifo(int TileId)
{
    tx_fifos.erase(TileId);
    rx_fifos.erase(TileId);
    for(int i = 0; i < valid_ids.size(); i++) {
        if(valid_ids[i] == TileId)
            valid_ids.erase(valid_ids.begin() + i);
    }
}

void
DeviceArbiter::handleFunctional(PacketPtr pkt)
{
    // Just pass this on to the memory side to handle for now.
    memPort.sendFunctional(pkt);
}

AddrRangeList
DeviceArbiter::getAddrRanges() const
{
    DPRINTF(DeviceArbiter, "Sending new ranges\n");
    // Just use the same ranges as whatever is on the memory side.
    return memPort.getAddrRanges();
}

void
DeviceArbiter::sendRangeChange()
{
    //instPort.sendRangeChange();
    dataPort.sendRangeChange();
}



DeviceArbiter*
DeviceArbiterParams::create()
{
    return new DeviceArbiter(this);
}
