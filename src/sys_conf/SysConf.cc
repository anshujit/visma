#include <vector>
#include "sys_conf/SysConf.hh"
//#include "sys_conf/tile.hh"
#include "mem/ruby/network/garnet2.0/GarnetNetwork.hh"
//#include "sim/eventq.hh"
#include "sys_conf/pkt_throttler.hh"

using namespace std;
//using m5::stl_helpers::deletePointers;

SysConf::SysConf(const Params *p)		//get parameters from GarnetNetwork
		: SimObject(p),
		  //requestTileEvent(this, false),
          enable(p->enable),
          arbiter(p->arbiter),
          numa_high_bit(p->numa_high_bit),
          ioXBar(p->io_xbar)
          //throttlers(p->throttlers)
          //io_startAddr(p->io_startAddr)
{
	if(isEnabled()) {
		GarnetNetwork *net = safe_cast<GarnetNetwork*> (p->network);
		m_routers = net->get_routers();	
		m_nodes = net->getNumNodes();
		m_nis = net->get_nis();
		m_rows = p->num_rows;                           //get the number of rows
		m_cols = m_routers.size() / m_rows;
		m_dma = net->getNumDmas();
		assert(m_rows >= 2);
		_tileID = 0;

		for(int i = 0; i < m_routers.size(); i++)
			links.push_back(m_routers[i]->getlinks());  //make a vector of all the links of each router

		for(int i = 0; i < m_routers.size(); i++)
			nbrs.push_back(m_routers[i]->getnbrs());

		m_luts.resize(m_nodes);
		routers.resize(m_rows);
		m_conns.resize(m_routers.size());

		for(int i = 0; i < m_nodes; i++)
			m_luts[i] = m_nis[i]->getLUT();

		//Set up the connBits with initial values
		for(int i = 0; i < m_routers.size(); i++)
			m_conns[i] = m_routers[i]->getconnBitsptr();
			
		//Create a matrix of routers with the elements being
		//either 0 or 1 depending on whether the router at
		//that index is free(1) or not(0). Initially, we
		//consider all the routers to be free(1).
		for(int i = 0; i < routers.size(); i++) {
			routers[i].resize(m_cols);
			for(int j = 0; j < routers[i].size(); j++)
				routers[i][j] = 1;
		}
        //curr_startAddr = io_startAddr;
        //arbiter->setThrottlers(p->throttlers);
	}
}

/**
  * This function creates a tile with the given no. of cores.
  * Here no. of routers = no. of cores (Mesh Topology).
  * The LUTs and connBits are set accordingly. 
  * This function should be called at runtime from the program
  * that will run in the cpu associated with this module.
  */

std::vector<int>
SysConf::requestTile(int num_cores, int max_pkts)
{
	//Check if the requested no. of cores is
	//more than the total cores in the system
	assert(isEnabled());
	
	if(num_cores > m_routers.size())
		fatal("Requested no. of cores is more than the total no. of cores in the system!!");
		
	/*else if(num_cores > )
		fatal("Requested no. of cores is more than the free cores available!!");*/

	else {
        //nextTileId();
		m_tile = new tile(m_conns, m_luts, _tileID);                    //create the tile object
		int tileCreated = m_tile->createTile(num_cores, routers);      //create the tile itself

		if(tileCreated == 1) {
            int currTileID = _tileID;                                    //if the tile is successfully created 
			//tiles.push_back(std::make_pair(_tileID, m_tile));			//include the new tile object in the vector of tiles
            tiles[_tileID] = m_tile;
			std::vector<int> router_ids = m_tile->getRouterIds();		//get the routers in the tile just created
			tileType = m_tile->getTileType();							//get the type of tile created
            std::vector<Throttler*> tile_throttlers;
			for(int i = 0; i < router_ids.size(); i++) {
				int x = router_ids[i] / m_cols;			//get the 2d coordinates from
				int y = router_ids[i] % m_cols;			//the 1d vector of routers.
				routers[x][y] = 0;						//reset the router indices used in the tile
				
				//Set/Reset the connBits for each router in the tile
				if(tileType == "Square-Shape")
					m_tile->SqTileconnBits(i, router_ids[i], links[router_ids[i]]);
				else if(tileType == "T-Shape")
					m_tile->TTileconnBits(i, router_ids[i], links[router_ids[i]]);
				else if(tileType == "Zig-Zag")
					m_tile->ZigZagTileconnBits(i, router_ids[i], links[router_ids[i]]);
				else
					m_tile->LTileconnBits(i, router_ids[i], links[router_ids[i]]);

				//Reset the connBits for each of the neighbour
				//routers which has output links into the tile
				m_tile->nbrconnBits(nbrs[router_ids[i]], router_ids[i], links);
				
				//Set/Reset the LUT bits accordingly
				m_tile->tilelutBits(m_dma, m_routers.size());
                //tile_throttlers.push_back(throttlers[router_ids[i]]);
			}
            
            ioXBar->setTileIds(router_ids, m_tile->get_id(), max_pkts);
            arbiter->make_valid(m_tile->get_id(), max_pkts);
            arbiter->setTileThrottlers(m_tile->get_id(), router_ids);
            //arbiter->deviceContext(m_tile->get_id(), AddrRange(curr_startAddr, curr_startAddr + 0x10));
            //mem_arbiter->make_valid(m_tile->get_id());
            m_tile->setBaseBoundAddr(numa_high_bit, m_routers.size());
            m_tile->setMidInvalidRange(numa_high_bit, m_routers.size());
			m_tile->print(cout);

            nextTileId();
            std::vector<int> return_object = router_ids;
            return_object.push_back(currTileID);
			return return_object;		
		}

		else if(tileCreated == -1)
	        return {-1};

        else
            return {-2};
	}
}

void
SysConf::nextTileId()
{
    int flag;
    //for(_tileID = 0;; _tileID++) {
    //    flag = 1;
    //    for(int i = 0; i < tiles.size(); i++) {
    //        if(_tileID == tiles[i].first) {
    //            flag = 0;
    //            break;
    //        }
    //    }
    //    if(flag == 1)
    //        break;
    //}
    for(_tileID = 0;; _tileID++) {
        flag = 1;
        for(std::map<int, tile *>::iterator x=tiles.begin(); x!=tiles.end(); ++x) {
            if(_tileID == x->first) {
                flag = 0;
                break;
            }
        }
        
        if(flag == 1)
            break;
    }
}

tile*
SysConf::getTile(int id)
{
    //for(int i = 0; i < tiles.size(); i++) {
    //    if(tiles[i].first == id)
    //        return tiles[i].second;
    //}

    for(std::map<int, tile *>::iterator x=tiles.begin(); x!=tiles.end(); ++x) {
       if(x->first == id)
            return tiles[id];
    } 
    return NULL;
}

std::vector<int>
SysConf::releaseTile(int id)
{
    int flag = 0;
	//for(int i = 0; i < tiles.size(); i++) {
	//	if(id == tiles[i].first) {
    //        flag = 1;
    //        tile_index = i;
	//		break;
	//    }
	//}
    for(std::map<int, tile *>::iterator x=tiles.begin(); x!=tiles.end(); ++x) {
       if(x->first == id)
           flag = 1;
    }

	if(flag == 0)
		fatal("The requested tile to release doesn't exist!!");

    //std::vector<int> router_ids = tiles[tile_index].second->getRouterIds();
    std::vector<int> router_ids = tiles[id]->getRouterIds();
    for(int i = 0; i < router_ids.size(); i++) {
        int x = router_ids[i] / m_cols;         //get the 2d coordinates from the 1d vector of routers
        int y = router_ids[i] % m_cols;
        routers[x][y] = 1;                      //set the router indices used in the tile which will free them
    }

    //std::vector<int> cores = tiles[tile_index].second->getRouterIds();
    std::vector<int> cores = tiles[id]->getRouterIds();
    //tiles.erase(tiles.begin() + tile_index);
    tiles.erase(id);
    arbiter->remove_fifo(id);
    nextTileId();
    return cores;
}

SysConf *
SysConfParams::create()
{
    return new SysConf(this);
}
