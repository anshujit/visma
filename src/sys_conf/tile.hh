#include <vector>
#include <string>
#include "base/types.hh"
#include "base/addr_range.hh"
#include "mem/ruby/network/garnet2.0/connBits.hh"
#include "mem/ruby/network/garnet2.0/LUT.hh"
#include "cpu/thread_context.hh"

class tile
{

	public:
		typedef std::vector<std::vector<int>> Matrix;
		tile(std::vector<connBits *> conns, std::vector<LUT *> luts, int id);

		int createTile(int num_cores, Matrix routers);
		std::vector<int> getRouterIds() { return router_ids; }
		std::string getTileType() { return type; }
		int get_id() { return tile_id; }

		void SqTileconnBits(int local_id, int global_id, std::vector<char> links);
		void TTileconnBits(int local_id, int global_id, std::vector<char> links);
		void ZigZagTileconnBits(int local_id, int global_id, std::vector<char> links);
		void LTileconnBits(int local_id, int global_id, std::vector<char> links);

		void nbrconnBits(std::vector<int> nbr_ids, int global_id, std::vector<std::vector<char>> allinks);
		
		void tilelutBits(int dmas, int num_routers);

        void setBaseBoundAddr(int numa_high_bit, int num_dirs);
        void setMidInvalidRange(int numa_high_bit, int num_dirs);
        Addr allocPhysPages(int npages);
        void registerThreadContext(ThreadContext *tc);
        ThreadContext *getThreadContext(int id) { /*inform("Getting ThreadContext ID:%d, in tile:%d\n", id, get_id())*/; return threadcontexts[id]; }

		bool findlink(char x, std::vector<char> links);
		bool findrouter(int id);
		void print(std::ostream& out) const;

	private:
		std::vector<int> router_ids;
		std::string type;
		std::vector<connBits *> m_conns;
		std::vector<LUT *> m_luts;
		std::vector<int> local_luts;
		int tile_id;
        Addr mask;
        Addr BaseAddrPtr;
        Addr BoundAddrPtr;
        std::vector<AddrRange> invalidranges;
        Addr pagePtr;
        int PageShift = 12;
        std::vector<ThreadContext *> threadcontexts;
};
