#include "mem/ruby/network/garnet2.0/LUT.hh"
//#include "mem/ruby/network/garnet2.0/GarnetNetwork.hh"
#include "mem/ruby/common/Set.hh"
#include <iostream>
#include <vector>


LUT::LUT()
{
	valid_nodes.resize(16);
}

void
LUT::setlut(int dmas, int L1s, int dirs)
{
	int num_cmp;
	for(int i = 0; i < valid_nodes.size(); i++) {
		if (i == 0)
			num_cmp = L1s;
		else if(i == 3)
			num_cmp = dirs;
		else if(i == 4)	
			num_cmp = dmas;
		else
			num_cmp = 0;

		for (int j = 0; j < num_cmp; j++) 
			valid_nodes[i].push_back(1);
	}
}

void
LUT::setlutBits(int id)
{
    assert(id >= 0);
	for(int i = 0; i < valid_nodes.size(); i++) {
		if(i == 0 || i == 3) {
            assert(id < valid_nodes[i].size());
			valid_nodes[i][id] = 1;
        }
	}
}

void
LUT::resetlutBits(int id)
{
    assert(id >= 0);
	for(int i = 0; i < valid_nodes.size(); i++) {
		if(i == 0 || i == 3) {
            assert(id < valid_nodes[i].size());
			valid_nodes[i][id] = 0;
	    }
    }
}

void
LUT::setlutdmaBits(int id)
{
    assert(id < valid_nodes[4].size());
    valid_nodes[4][id] = 1;
}

void
LUT::resetlutdmaBits(int id)
{
    assert(id < valid_nodes[4].size());
    valid_nodes[4][id] = 0;
}

bool
LUT::isAllowed(NetDest msg_dest)
{
	std::vector<Set> m_bits = msg_dest.get_m_bits();
	
	assert(valid_nodes.size() == m_bits.size());
	
	std::vector<std::vector<int>> result;
	result.resize(m_bits.size());

	for(int i = 0; i < m_bits.size(); i++)
	{
		for (int j = 0; j < m_bits[i].getSize(); j++)
		{
			assert(valid_nodes[i].size() == m_bits[i].getSize());
			result[i].resize(m_bits[i].getSize());
			
			result[i][j] = valid_nodes[i][j] & m_bits[i].isElement(j);
			if(result[i][j] != m_bits[i].isElement(j))
			{
				//std::cout << m_bits[i].isElement(j) << std::endl;
				return false;
			} 		
		}
	}
				
	return true;
}

void
LUT::lutversion(int id)
{
    _LUTid = id;
}

void
LUT::tileid(int id)
{
    _tileId = id;
}

void
LUT::print(std::ostream& out) const
{
	out << "[(" << valid_nodes.size() << ")";
	for(int i = 0; i < valid_nodes.size(); i++) {
		for (int j = 0; j < valid_nodes[i].size(); j++)
			out << " " << valid_nodes[i][j];
		out << " -";
	}
	out << "]";
}
