#ifndef __MEM_RUBY_NETWORK_GARNET_FIXED_PIPELINE_LUT_HH__
#define __MEM_RUBY_NETWORK_GARNET_FIXED_PIPELINE_LUT_HH__

#include "mem/ruby/common/NetDest.hh"
#include "mem/ruby/common/Set.hh"
#include <vector>

class LUT
{
	public:
		LUT();

		bool isAllowed (NetDest msg_dest);
		std::vector<std::vector<int>> getvalidnodes() { return valid_nodes; }
		void setlut(int dmas, int L1s, int dirs);

		void setlutBits(int id);
		void resetlutBits(int id);
        void setlutdmaBits(int id);
		void resetlutdmaBits(int id);
		void print(std::ostream& out) const;
        void lutversion(int id);
        void tileid(int id);

        int _LUTid = -1;
        int _tileId = -1;
	private:
		std::vector<Set> m_bits;		
		//int m_nodes;

		//int num_routers;
		std::vector<std::vector<int>> valid_nodes;
};
#endif
