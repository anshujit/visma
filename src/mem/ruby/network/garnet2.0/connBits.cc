
#include "mem/ruby/network/garnet2.0/connBits.hh"
#include <vector>
using namespace std;

connBits::connBits()
{
}
 
void
connBits::setconnBits(int size)
{
	for (int i = 0; i < size; i++)
		Bits.push_back(1);
}

void
connBits::setconnBit(int index)
{
	Bits[index] = 1;
}

void
connBits::resetconnBit(int index)
{
	Bits[index] = 0;
}

bool
connBits::isValid(int link_id)
{
	return Bits[link_id] == 1;
}

void
connBits::print(std::ostream& out) const
{
	for(int i = 0; i < Bits.size(); i++)
		out << Bits[i] << " ";
}
