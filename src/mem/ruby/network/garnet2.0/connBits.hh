
#ifndef __MEM_RUBY_NETWORK_GARNET_FIXED_PIPELINE_CONNBITS_REGISTER_HH__
#define __MEM_RUBY_NETWORK_GARNET_FIXED_PIPELINE_CONNBITS_REGISTER_HH__

#include <vector>
#include <iostream>
class connBits
{
	public:
 
		connBits(); 
		std::vector<int> getconnBits(){	return Bits; }
		void setconnBits(int size);
		void setconnBit(int index);
		void resetconnBit(int index);
		bool isValid(int link_id);
		void print(std::ostream& out) const;

	private:

		std::vector<int> Bits;
		
};
#endif
