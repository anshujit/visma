# Copyright (c) 2012-2013 ARM Limited
# All rights reserved.
#
# The license below extends only to copyright in the software and shall
# not be construed as granting a license to any other intellectual
# property including but not limited to intellectual property relating
# to a hardware implementation of the functionality of the software
# licensed hereunder.  You may use the software subject to the license
# terms below provided that you ensure that this notice is replicated
# unmodified and in its entirety in all distributions of the software,
# modified or unmodified, in source code or in binary form.
#
# Copyright (c) 2006-2008 The Regents of The University of Michigan
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met: redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer;
# redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution;
# neither the name of the copyright holders nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# Authors: Steve Reinhardt

# Simple test script
#
# "m5 test.py"

import optparse
import sys
import os

import m5
from m5.defines import buildEnv
from m5.objects import *
from m5.util import addToPath, fatal

addToPath('../')

from ruby import Ruby

from common import Options
from common import Simulation
from common import CacheConfig
from common import CpuConfig
from common import MemConfig
from common.Caches import *
from common.cpu2000 import *

# Check if KVM support has been enabled, we might need to do VM
# configuration if that's the case.
have_kvm_support = 'BaseKvmCPU' in globals()
def is_kvm_cpu(cpu_class):
    return have_kvm_support and cpu_class != None and \
        issubclass(cpu_class, BaseKvmCPU)

def get_processes(options):
    """Interprets provided options and returns a list of processes"""

    multiprocesses = []
    inputs = []
    outputs = []
    errouts = []
    pargs = []

    workloads = options.cmd.split(';')
    if options.input != "":
        inputs = options.input.split(';')
    if options.output != "":
        outputs = options.output.split(';')
    if options.errout != "":
        errouts = options.errout.split(';')
    if options.options != "":
        pargs = options.options.split(';')

    idx = 0
    for wrkld in workloads:
        process = LiveProcess()
        process.executable = wrkld
        process.cwd = os.getcwd()

        if options.env:
            with open(options.env, 'r') as f:
                process.env = [line.rstrip() for line in f]

        if len(pargs) > idx:
            process.cmd = [wrkld] + pargs[idx].split()
        else:
            process.cmd = [wrkld]

        if len(inputs) > idx:
            process.input = inputs[idx]
        if len(outputs) > idx:
            process.output = outputs[idx]
        if len(errouts) > idx:
            process.errout = errouts[idx]

        multiprocesses.append(process)
        idx += 1

    if options.smt:
        assert(options.cpu_type == "detailed")
        return multiprocesses, idx
    else:
        return multiprocesses, 1


parser = optparse.OptionParser()
Options.addCommonOptions(parser)
Options.addSEOptions(parser)

if '--ruby' in sys.argv:
    Ruby.define_options(parser)

(options, args) = parser.parse_args()

if args:
    print "Error: script doesn't take any positional arguments"
    sys.exit(1)

multiprocesses = []
numThreads = 1

if options.bench:
    apps = options.bench.split("-")
    if len(apps) != options.num_cpus:
        print "number of benchmarks not equal to set num_cpus!"
        sys.exit(1)

    for app in apps:
        try:
            if buildEnv['TARGET_ISA'] == 'alpha':
                exec("workload = %s('alpha', 'tru64', '%s')" % (
                        app, options.spec_input))
            elif buildEnv['TARGET_ISA'] == 'arm':
                exec("workload = %s('arm_%s', 'linux', '%s')" % (
                        app, options.arm_iset, options.spec_input))
            else:
                exec("workload = %s(buildEnv['TARGET_ISA', 'linux', '%s')" % (
                        app, options.spec_input))
            multiprocesses.append(workload.makeLiveProcess())
        except:
            print >>sys.stderr, "Unable to find workload for %s: %s" % (
                    buildEnv['TARGET_ISA'], app)
            sys.exit(1)
elif options.cmd:
    multiprocesses, numThreads = get_processes(options)
else:
    print >> sys.stderr, "No workload specified. Exiting!\n"
    sys.exit(1)


(CPUClass, test_mem_mode, FutureClass) = Simulation.setCPUClass(options)
CPUClass.numThreads = numThreads

# Check -- do not allow SMT with multiple CPUs
if options.smt and options.num_cpus > 1:
    fatal("You cannot use SMT with multiple CPUs!")

np = options.num_cpus
system1 = System(cpu = [CPUClass(cpu_id=i) for i in xrange(np)],
                mem_mode = test_mem_mode,
                mem_ranges = [AddrRange(options.mem_size)],
                cache_line_size = options.cacheline_size)

##############system2 with System Configurator###################
system2 = System()

# Create the SysConf
system2.sys_conf.enable = True
system2.sys_conf.num_rows = options.mesh_rows

system2.clk_domain = SrcClockDomain()
system2.clk_domain.clock = '1GHz'
system2.clk_domain.voltage_domain = VoltageDomain()
"""
# Set up the system
system2.mem_mode = 'timing'               # Use timing accesses
system2.mem_ranges = [AddrRange('512MB')] # Create an address range

# Create a simple CPU
system2.cpu = TimingSimpleCPU(cpu_id = 0)
"""
# Create a memory bus, a system crossbar, in this case
system2.membus = SystemXBar()
"""
# Hook the CPU ports up to the membus
system2.cpu.icache_port = system2.membus.slave
system2.cpu.dcache_port = system2.membus.slave

# create the interrupt controller for the CPU and connect to the membus
system2.cpu.createInterruptController()

# For x86 only, make sure the interrupts are connected to the memory
# Note: these are directly connected to the memory bus and are not cached
if m5.defines.buildEnv['TARGET_ISA'] == "x86":
    system2.cpu.interrupts[0].pio = system2.membus.master
    system2.cpu.interrupts[0].int_master = system2.membus.slave
    system2.cpu.interrupts[0].int_slave = system2.membus.master

# Create a DDR3 memory controller and connect it to the membus
system2.mem_ctrl = DDR3_1600_x64()
system2.mem_ctrl.range = system2.mem_ranges[0]
system2.mem_ctrl.port = system2.membus.master
"""
# Connect the system up to the membus
system2.system_port = system2.membus.slave
"""
# get ISA for the binary to run.
isa = str(m5.defines.buildEnv['TARGET_ISA']).lower()

# Run 'hello' and use the compiled ISA to find the binary
#binary = 'tests/test-progs/hello/bin/' + isa + '/linux/hello'
binary = 'tests/test-progs/my_test/vm'

# Create a process for a simple "Hello World" application
process2 = LiveProcess()
# Set the command
# cmd is a list which begins with the executable (like argv)
process2.cmd = [binary]
# Set the cpu to use the process as its workload and create thread contexts
system2.cpu.workload = process2
system2.cpu.createThreads()"""
#################################################################

if numThreads > 1:
    system1.multi_thread = True

# Create a top-level voltage domain
system1.voltage_domain = VoltageDomain(voltage = options.sys_voltage)

# Create a source clock for the system and set the clock period
system1.clk_domain = SrcClockDomain(clock =  options.sys_clock,
                                   voltage_domain = system1.voltage_domain)

# Create a CPU voltage domain
system1.cpu_voltage_domain = VoltageDomain()

# Create a separate clock domain for the CPUs
system1.cpu_clk_domain = SrcClockDomain(clock = options.cpu_clock,
                                       voltage_domain =
                                       system1.cpu_voltage_domain)

# If elastic tracing is enabled, then configure the cpu and attach the elastic
# trace probe
if options.elastic_trace_en:
    CpuConfig.config_etrace(CPUClass, system1.cpu, options)

# All cpus belong to a common cpu_clk_domain, therefore running at a common
# frequency.
for cpu in system1.cpu:
    cpu.clk_domain = system1.cpu_clk_domain

if is_kvm_cpu(CPUClass) or is_kvm_cpu(FutureClass):
    if buildEnv['TARGET_ISA'] == 'x86':
        system1.vm = KvmVM()
        for process in multiprocesses:
            process.useArchPT = True
            process.kvmInSE = True
    else:
        fatal("KvmCPU can only be used in SE mode with x86")

# Sanity check
if options.fastmem:
    if CPUClass != AtomicSimpleCPU:
        fatal("Fastmem can only be used with atomic CPU!")
    if (options.caches or options.l2cache):
        fatal("You cannot use fastmem in combination with caches!")

if options.simpoint_profile:
    if not options.fastmem:
        # Atomic CPU checked with fastmem option already
        fatal("SimPoint generation should be done with atomic cpu and fastmem")
    if np > 1:
        fatal("SimPoint generation not supported with more than one CPUs")

for i in xrange(np):
    if options.smt:
        system1.cpu[i].workload = multiprocesses
    elif len(multiprocesses) == 1:
        system1.cpu[i].workload = multiprocesses[0]
    else:
        system1.cpu[i].workload = multiprocesses[i]

    if options.fastmem:
        system1.cpu[i].fastmem = True

    if options.simpoint_profile:
        system1.cpu[i].addSimPointProbe(options.simpoint_interval)

    if options.checker:
        system1.cpu[i].addCheckerCpu()

    system1.cpu[i].createThreads()

if options.ruby:
    if options.cpu_type == "atomic" or options.cpu_type == "AtomicSimpleCPU":
        print >> sys.stderr, "Ruby does not work with atomic cpu!!"
        sys.exit(1)

    network = Ruby.create_system(options, False, system1)
    system2.sys_conf.network = network
    assert(options.num_cpus == len(system1.ruby._cpu_ports))

    system1.ruby.clk_domain = SrcClockDomain(clock = options.ruby_clock,
                                        voltage_domain = system1.voltage_domain)
    for i in xrange(np):
        ruby_port = system1.ruby._cpu_ports[i]

        # Create the interrupt controller and connect its ports to Ruby
        # Note that the interrupt controller is always present but only
        # in x86 does it have message ports that need to be connected
        system1.cpu[i].createInterruptController()

        # Connect the cpu's cache ports to Ruby
        system1.cpu[i].icache_port = ruby_port.slave
        system1.cpu[i].dcache_port = ruby_port.slave
        if buildEnv['TARGET_ISA'] == 'x86':
            system1.cpu[i].interrupts[0].pio = ruby_port.master
            system1.cpu[i].interrupts[0].int_master = ruby_port.slave
            system1.cpu[i].interrupts[0].int_slave = ruby_port.master
            system1.cpu[i].itb.walker.port = ruby_port.slave
            system1.cpu[i].dtb.walker.port = ruby_port.slave
else:
    MemClass = Simulation.setMemClass(options)
    system1.membus = SystemXBar()
    system1.system_port = system1.membus.slave
    CacheConfig.config_cache(options, system1)
    MemConfig.config_mem(options, system1)

root = Root(full_system = False, system1 = system1, system2 = system2)
m5.instantiate()
print "**********Beginning simulation**********"
exit_event = m5.simulate()
print 'Exiting @ tick %i because %s' % (m5.curTick(), exit_event.getCause())
