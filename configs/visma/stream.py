# Copyright (c) 2012-2013 ARM Limited
# All rights reserved.
#
# The license below extends only to copyright in the software and shall
# not be construed as granting a license to any other intellectual
# property including but not limited to intellectual property relating
# to a hardware implementation of the functionality of the software
# licensed hereunder.  You may use the software subject to the license
# terms below provided that you ensure that this notice is replicated
# unmodified and in its entirety in all distributions of the software,
# modified or unmodified, in source code or in binary form.
#
# Copyright (c) 2006-2008 The Regents of The University of Michigan
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met: redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer;
# redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution;
# neither the name of the copyright holders nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# Authors: Steve Reinhardt

# Simple test script
#
# "m5 test.py"

import optparse
import sys
import os
import time
import sip

import m5
from m5.defines import buildEnv
from m5.objects import *
from m5.util import addToPath, fatal
from multiprocessing.managers import BaseManager, NamespaceProxy, BaseProxy
import subsystem

addToPath('../')

from ruby import Ruby

from common import Options
from common import Simulation
from common import CacheConfig
from common import CpuConfig
from common import MemConfig
from common.Caches import *
from common.cpu2000 import *

# Check if KVM support has been enabled, we might need to do VM
# configuration if that's the case.
have_kvm_support = 'BaseKvmCPU' in globals()
def is_kvm_cpu(cpu_class):
    return have_kvm_support and cpu_class != None and \
        issubclass(cpu_class, BaseKvmCPU)

def get_processes(options):
    """Interprets provided options and returns a list of processes"""

    multiprocesses = []
    inputs = []
    outputs = []
    errouts = []
    pargs = []

    workloads = options.cmd.split(';')
    if options.input != "":
        inputs = options.input.split(';')
    if options.output != "":
        outputs = options.output.split(';')
    if options.errout != "":
        errouts = options.errout.split(';')
    if options.options != "":
        pargs = options.options.split(';')

    idx = 0
    for wrkld in workloads:
        process = LiveProcess()
        process.executable = wrkld
        process.cwd = os.getcwd()

        if options.env:
            with open(options.env, 'r') as f:
                process.env = [line.rstrip() for line in f]

        if len(pargs) > idx:
            process.cmd = [wrkld] + pargs[idx].split()
        else:
            process.cmd = [wrkld]

        if len(inputs) > idx:
            process.input = inputs[idx]
        if len(outputs) > idx:
            process.output = outputs[idx]
        if len(errouts) > idx:
            process.errout = errouts[idx]

        multiprocesses.append(process)
        idx += 1

    if options.smt:
        assert(options.cpu_type == "detailed")
        return multiprocesses, idx
    else:
        return multiprocesses, 1


parser = optparse.OptionParser()
Options.addCommonOptions(parser)
Options.addSEOptions(parser)

if '--ruby' in sys.argv:
    Ruby.define_options(parser)

(options, args) = parser.parse_args()

if args:
    print "Error: script doesn't take any positional arguments"
    sys.exit(1)

multiprocesses = []
numThreads = 1

if options.bench:
    apps = options.bench.split("-")
    if len(apps) != options.num_cpus:
        print "number of benchmarks not equal to set num_cpus!"
        sys.exit(1)

    for app in apps:
        try:
            if buildEnv['TARGET_ISA'] == 'alpha':
                exec("workload = %s('alpha', 'tru64', '%s')" % (
                        app, options.spec_input))
            elif buildEnv['TARGET_ISA'] == 'arm':
                exec("workload = %s('arm_%s', 'linux', '%s')" % (
                        app, options.arm_iset, options.spec_input))
            else:
                exec("workload = %s(buildEnv['TARGET_ISA', 'linux', '%s')" % (
                        app, options.spec_input))
            multiprocesses.append(workload.makeLiveProcess())
        except:
            print >>sys.stderr, "Unable to find workload for %s: %s" % (
                    buildEnv['TARGET_ISA'], app)
            sys.exit(1)
elif options.cmd:
    multiprocesses, numThreads = get_processes(options)
else:
    print >> sys.stderr, "No workload specified. Exiting!\n"
    sys.exit(1)


(CPUClass, test_mem_mode, FutureClass) = Simulation.setCPUClass(options)
CPUClass.numThreads = numThreads

# Check -- do not allow SMT with multiple CPUs
if options.smt and options.num_cpus > 1:
    fatal("You cannot use SMT with multiple CPUs!")

np = options.num_cpus
system1 = System(cpu = [CPUClass(cpu_id=i) for i in xrange(np)],
                mem_mode = test_mem_mode,
                mem_ranges = [AddrRange(options.mem_size)],
                              #AddrRange(options.mem_size, size = 0x100000)],
                cache_line_size = options.cacheline_size)

##################################################################
system2 = System()

system2.sys_conf = SysConf()
system2.sys_conf.enable = True
system2.sys_conf.num_rows = options.mesh_rows
system2.sys_conf.numa_high_bit = options.numa_high_bit

system2.clk_domain = SrcClockDomain()
system2.clk_domain.clock = '1GHz'
system2.clk_domain.voltage_domain = VoltageDomain()

# Set up the system
system2.mem_mode = 'timing'               # Use timing accesses
system2.mem_ranges = [AddrRange('512MB')] # Create an address range

# Create a simple CPU
system2.cpu = TimingSimpleCPU(cpu_id = 0)

# Create a memory bus, a system crossbar, in this case
system2.membus = SystemXBar()

# Hook the CPU ports up to the membus
system2.cpu.icache_port = system2.membus.slave
system2.cpu.dcache_port = system2.membus.slave

# create the interrupt controller for the CPU and connect to the membus
system2.cpu.createInterruptController()

# For x86 only, make sure the interrupts are connected to the memory
# Note: these are directly connected to the memory bus and are not cached
if m5.defines.buildEnv['TARGET_ISA'] == "x86":
    system2.cpu.interrupts[0].pio = system2.membus.master
    system2.cpu.interrupts[0].int_master = system2.membus.slave
    system2.cpu.interrupts[0].int_slave = system2.membus.master

# Create a DDR3 memory controller and connect it to the membus
system2.mem_ctrl = DDR3_1600_x64()
system2.mem_ctrl.range = system2.mem_ranges[0]
system2.mem_ctrl.port = system2.membus.master

# Connect the system up to the membus
system2.system_port = system2.membus.slave

# get ISA for the binary to run.
isa = str(m5.defines.buildEnv['TARGET_ISA']).lower()

# Run 'hello' and use the compiled ISA to find the binary
#binary = 'tests/test-progs/hello/bin/' + isa + '/linux/hello'
binary = 'tests/test-progs/my_test/vm_test'

# Create a process for a simple "Hello World" application
process2 = LiveProcess()
# Set the command
# cmd is a list which begins with the executable (like argv)
process2.cmd = [binary]
# Set the cpu to use the process as its workload and create thread contexts
system2.cpu.workload = process2
system2.cpu.createThreads()
##################################################################

if numThreads > 1:
    system1.multi_thread = True

# Create a top-level voltage domain
system1.voltage_domain = VoltageDomain(voltage = options.sys_voltage)

# Create a source clock for the system and set the clock period
system1.clk_domain = SrcClockDomain(clock =  options.sys_clock,
                                   voltage_domain = system1.voltage_domain)

# Create a CPU voltage domain
system1.cpu_voltage_domain = VoltageDomain()

# Create a separate clock domain for the CPUs
system1.cpu_clk_domain = SrcClockDomain(clock = options.cpu_clock,
                                       voltage_domain =
                                       system1.cpu_voltage_domain)

# If elastic tracing is enabled, then configure the cpu and attach the elastic
# trace probe
if options.elastic_trace_en:
    CpuConfig.config_etrace(CPUClass, system1.cpu, options)

# All cpus belong to a common cpu_clk_domain, therefore running at a common
# frequency.
for cpu in system1.cpu:
    cpu.clk_domain = system1.cpu_clk_domain

if is_kvm_cpu(CPUClass) or is_kvm_cpu(FutureClass):
    if buildEnv['TARGET_ISA'] == 'x86':
        system1.vm = KvmVM()
        for process in multiprocesses:
            process.useArchPT = True
            process.kvmInSE = True
    else:
        fatal("KvmCPU can only be used in SE mode with x86")

# Sanity check
if options.fastmem:
    if CPUClass != AtomicSimpleCPU:
        fatal("Fastmem can only be used with atomic CPU!")
    if (options.caches or options.l2cache):
        fatal("You cannot use fastmem in combination with caches!")

if options.simpoint_profile:
    if not options.fastmem:
        # Atomic CPU checked with fastmem option already
        fatal("SimPoint generation should be done with atomic cpu and fastmem")
    if np > 1:
        fatal("SimPoint generation not supported with more than one CPUs")

###############################################################################################################
class STREAM(LiveProcess):
    cwd = '/home/anshujit/project/Benchmarks/STREAM'
    executable = '/home/anshujit/project/Benchmarks/STREAM/stream'
    cmd = ['stream']

class STREAM_io(LiveProcess):
    cwd = '/home/anshujit/project/Benchmarks/STREAM'
    executable = '/home/anshujit/project/Benchmarks/STREAM/stream_io'
    cmd = ['stream_io']
class STREAM_io1(LiveProcess):
    cwd = '/home/anshujit/project/Benchmarks/STREAM'
    executable = '/home/anshujit/project/Benchmarks/STREAM/stream_io1'
    cmd = ['stream_io1']
class STREAM_io2(LiveProcess):
    cwd = '/home/anshujit/project/Benchmarks/STREAM'
    executable = '/home/anshujit/project/Benchmarks/STREAM/stream_io2'
    cmd = ['stream_io2']
class STREAM_io3(LiveProcess):
    cwd = '/home/anshujit/project/Benchmarks/STREAM'
    executable = '/home/anshujit/project/Benchmarks/STREAM/stream_io3'
    cmd = ['stream_io3']
class STREAM_io4(LiveProcess):
    cwd = '/home/anshujit/project/Benchmarks/STREAM'
    executable = '/home/anshujit/project/Benchmarks/STREAM/stream_io4'
    cmd = ['stream_io4']
class STREAM_io5(LiveProcess):
    cwd = '/home/anshujit/project/Benchmarks/STREAM'
    executable = '/home/anshujit/project/Benchmarks/STREAM/stream_io5'
    cmd = ['stream_io5']
class STREAM_io6(LiveProcess):
    cwd = '/home/anshujit/project/Benchmarks/STREAM'
    executable = '/home/anshujit/project/Benchmarks/STREAM/stream_io6'
    cmd = ['stream_io6']
class STREAM_io7(LiveProcess):
    cwd = '/home/anshujit/project/Benchmarks/STREAM'
    executable = '/home/anshujit/project/Benchmarks/STREAM/stream_io7'
    cmd = ['stream_io7']

class nbench(LiveProcess):
    cwd = '/home/anshujit/project/Benchmarks/nbench-byte-2.2.3'
    executable = '/home/anshujit/project/Benchmarks/nbench-byte-2.2.3/nbench'
    cmd = ['nbench']

class FFT(LiveProcess):
    cwd = '/home/anshujit/project/Benchmarks/splash2/splash2/codes/kernels/fft'
    executable = '/home/anshujit/project/Benchmarks/splash2/splash2/codes/kernels/fft/FFT'
    cmd = ['FFT']

class LU_c(LiveProcess):
    cwd = '/home/anshujit/project/Benchmarks/splash2/splash2/codes/kernels/lu/contiguous_blocks'
    executable = '/home/anshujit/project/Benchmarks/splash2/splash2/codes/kernels/lu/contiguous_blocks/LU'
    cmd = ['LU']

class LU_nc(LiveProcess):
    cwd = '/home/anshujit/project/Benchmarks/splash2/splash2/codes/kernels/lu/non_contiguous_blocks'
    executable = '/home/anshujit/project/Benchmarks/splash2/splash2/codes/kernels/lu/non_contiguous_blocks/LU'
    cmd = ['LU']

class Cholesky(LiveProcess):
    cwd = '/home/anshujit/project/Benchmarks/splash2/splash2/codes/kernels/cholesky'
    executable = '/home/anshujit/project/Benchmarks/splash2/splash2/codes/kernels/cholesky/CHOLESKY'
    input = '/home/anshujit/project/Benchmarks/splash2/splash2/codes/kernels/cholesky/inputs/lshp.O'
    cmd = ['CHOLESKY']

class Radix(LiveProcess):
    cwd = '/home/anshujit/project/Benchmarks/splash2/splash2/codes/kernels/radix'
    executable = '/home/anshujit/project/Benchmarks/splash2/splash2/codes/kernels/radix/RADIX'
    cmd = ['RADIX']

class Barnes(LiveProcess):
    cwd = '/home/anshujit/project/Benchmarks/splash2/splash2/codes/apps/barnes'
    executable = '/home/anshujit/project/Benchmarks/splash2/splash2/codes/apps/barnes/BARNES'
    input = '/home/anshujit/project/Benchmarks/splash2/splash2/codes/apps/barnes/input2'
    cmd = ['BARNES']

class Ocean_c(LiveProcess):
    cwd = '/home/anshujit/project/Benchmarks/splash2/splash2/codes/apps/ocean/contiguous_partitions'
    executable = '/home/anshujit/project/Benchmarks/splash2/splash2/codes/apps/ocean/contiguous_partitions/OCEAN'
    cmd = ['OCEAN']

class Ocean_nc(LiveProcess):
    cwd = '/home/anshujit/project/Benchmarks/splash2/splash2/codes/apps/ocean/non_contiguous_partitions'
    executable = '/home/anshujit/project/Benchmarks/splash2/splash2/codes/apps/ocean/non_contiguous_partitions/OCEAN'
    cmd = ['OCEAN']

class FMM(LiveProcess):
    cwd = '/home/anshujit/project/Benchmarks/splash2/splash2/codes/apps/fmm'
    executable = '/home/anshujit/project/Benchmarks/splash2/splash2/codes/apps/fmm/FMM'
    input = '/home/anshujit/project/Benchmarks/splash2/splash2/codes/apps/fmm/inputs/input.256'
    cmd = ['FMM']

class Radiosity(LiveProcess):#
    cwd = '/home/anshujit/project/Benchmarks/splash2/splash2/codes/apps/radiosity'
    executable = '/home/anshujit/project/Benchmarks/splash2/splash2/codes/apps/radiosity/RADIOSITY'
    cmd = ['RADIOSITY', '-p', ' 4', '-tq', ' 10', '-ae', ' 1000000000', '-batch', '-room']

class Raytrace(LiveProcess):#
    cwd = '/home/anshujit/project/Benchmarks/splash2/splash2/codes/apps/raytrace'
    executable = '/home/anshujit/project/Benchmarks/splash2/splash2/codes/apps/raytrace/RAYTRACE'
    #input = '/home/anshujit/project/Benchmarks/splash2/splash2/codes/apps/raytrace/inputs/teapot.env'
    cmd = ['RAYTRACE', '-p4', '-m64', '/home/anshujit/project/Benchmarks/splash2/splash2/codes/apps/raytrace/inputs/teapot.env']

#####Not working, buffer overflow issue, trying to open some file#####
class Volrend(LiveProcess):
    cwd = '/home/anshujit/project/Benchmarks/splash2/splash2/codes/apps/volrend'
    executable = '/home/anshujit/project/Benchmarks/splash2/splash2/codes/apps/volrend/VOLREND'
    cmd = ['VOLREND', '1', '/home/anshujit/project/Benchmarks/splash2/splash2/codes/apps/volrend/inputs/head']
######################################################################

class Water_nsquared(LiveProcess):#
    cwd = '/home/anshujit/project/Benchmarks/splash2/splash2/codes/apps/water-nsquared'
    executable = '/home/anshujit/project/Benchmarks/splash2/splash2/codes/apps/water-nsquared/WATER-NSQUARED'
    input = '/home/anshujit/project/Benchmarks/splash2/splash2/codes/apps/water-nsquared/input2'
    cmd = ['WATER-NSQUARED']

class Water_spatial(LiveProcess):#
    cwd = '/home/anshujit/project/Benchmarks/splash2/splash2/codes/apps/water-spatial'
    executable = '/home/anshujit/project/Benchmarks/splash2/splash2/codes/apps/water-spatial/WATER-SPATIAL'
    input = '/home/anshujit/project/Benchmarks/splash2/splash2/codes/apps/water-spatial/input2'
    cmd = ['WATER-SPATIAL']
###############################################################################################################
'''
process1 = STREAM_io()
process2 = STREAM_io1()
process3 = STREAM_io2()
process4 = STREAM_io3()
process5 = STREAM_io4()
process6 = STREAM_io5()
process7 = STREAM_io6()
process8 = STREAM_io7()
process9 = STREAM_io()
process10 = STREAM_io()
process11 = STREAM_io()
process12 = STREAM_io()
process13 = STREAM_io()
process14 = STREAM_io()
process15 = STREAM_io()
process16 = STREAM_io()

system1.cpu[0].workload = multiprocesses[0]
system1.cpu[1].workload = multiprocesses[1]
system1.cpu[4].workload = multiprocesses[2]
system1.cpu[5].workload = multiprocesses[3]
system1.cpu[2].workload = multiprocesses[4]
system1.cpu[3].workload = multiprocesses[5]
system1.cpu[6].workload = multiprocesses[6]
system1.cpu[7].workload = multiprocesses[7]
system1.cpu[8].workload = multiprocesses[8]
system1.cpu[9].workload = multiprocesses[9]
system1.cpu[12].workload = multiprocesses[10]
system1.cpu[13].workload = multiprocesses[11]
system1.cpu[10].workload = multiprocesses[12]
system1.cpu[11].workload = multiprocesses[13]
system1.cpu[14].workload = multiprocesses[14]
system1.cpu[15].workload = multiprocesses[15]

system1.cpu[0].workload = multiprocesses[0]
system1.cpu[1].workload = multiprocesses[0]
system1.cpu[4].workload = multiprocesses[0]
system1.cpu[5].workload = multiprocesses[0]
system1.cpu[2].workload = process5
system1.cpu[3].workload = process6
system1.cpu[6].workload = process7
system1.cpu[7].workload = process8
system1.cpu[8].workload = process9
system1.cpu[9].workload = process10
system1.cpu[12].workload = process11
system1.cpu[13].workload = process12
system1.cpu[10].workload = process13
system1.cpu[11].workload = process14
system1.cpu[14].workload = process15
system1.cpu[15].workload = process16
'''
process1 = FFT()
process2 = FFT()
process3 = FFT()
process4 = FFT()

system1.cpu[0].workload = process1
system1.cpu[1].workload = process1
system1.cpu[4].workload = process1
system1.cpu[5].workload = process1
system1.cpu[2].workload = process2
system1.cpu[3].workload = process2
system1.cpu[6].workload = process2
system1.cpu[7].workload = process2
system1.cpu[8].workload = process3
system1.cpu[9].workload = process3
system1.cpu[12].workload = process3
system1.cpu[13].workload = process3
system1.cpu[10].workload = process4
system1.cpu[11].workload = process4
system1.cpu[14].workload = process4
system1.cpu[15].workload = process4

for i in xrange(np):
    system1.cpu[i].workload[0].sys_conf = system2.sys_conf
##########################################################
for i in xrange(np):
    ''' if options.smt:
        system1.cpu[i].workload = multiprocesses
    elif len(multiprocesses) == 1:
        system1.cpu[i].workload = multiprocesses[0]
    else:
        system1.cpu[i].workload = multiprocesses[i]
    '''
    if options.fastmem:
        system1.cpu[i].fastmem = True

    if options.simpoint_profile:
        system1.cpu[i].addSimPointProbe(options.simpoint_interval)

    if options.checker:
        system1.cpu[i].addCheckerCpu()

    system1.cpu[i].createThreads()

if options.ruby:
    if options.cpu_type == "atomic" or options.cpu_type == "AtomicSimpleCPU":
        print >> sys.stderr, "Ruby does not work with atomic cpu!!"
        sys.exit(1)

    system1.piobus = ModiIOXBar()
    (network, throttlers) = Ruby.create_system(options, False, system1, system1.piobus)
    system2.sys_conf.network = network
    #system2.sys_conf.cpu_seqs = cpu_seqs

    system1.piobus.io_arbiter = DeviceArbiter()
    system1.piobus.io_arbiter.recv_req_quantum = 180000
    system1.piobus.io_arbiter.tx_req_quantum = 20000
    system1.piobus.io_arbiter.recv_resp_quantum = 180000
    system1.piobus.io_arbiter.send_resp_quantum = 20000
    system1.piobus.io_arbiter.latency = 1000
    system1.piobus.io_arbiter.throttlers = throttlers
    system2.sys_conf.arbiter = system1.piobus.io_arbiter
    system2.sys_conf.io_xbar = system1.piobus

    system1.piobus.isa_fake = IsaFake()
    system1.piobus.isa_fake.pio_addr = "0xAAAAAA0000"
    system1.piobus.isa_fake.pio_size = "0x40000"
    #system1.piobus.isa_fake.warn_access = "Device accessed"
    
    system1.piobus.master = system1.piobus.io_arbiter.data_port
    system1.piobus.io_arbiter.mem_side = system1.piobus.isa_fake.pio

    assert(options.num_cpus == len(system1.ruby._cpu_ports))

    system1.ruby.clk_domain = SrcClockDomain(clock = options.ruby_clock,
                                        voltage_domain = system1.voltage_domain)
    for i in xrange(np):
        ruby_port = system1.ruby._cpu_ports[i]

        # Create the interrupt controller and connect its ports to Ruby
        # Note that the interrupt controller is always present but only
        # in x86 does it have message ports that need to be connected
        system1.cpu[i].createInterruptController()

        # Connect the cpu's cache ports to Ruby
        system1.cpu[i].icache_port = ruby_port.slave
        system1.cpu[i].dcache_port = ruby_port.slave
        if buildEnv['TARGET_ISA'] == 'x86':
            system1.cpu[i].interrupts[0].pio = ruby_port.master
            system1.cpu[i].interrupts[0].int_master = ruby_port.slave
            system1.cpu[i].interrupts[0].int_slave = ruby_port.master
            system1.cpu[i].itb.walker.port = ruby_port.slave
            system1.cpu[i].dtb.walker.port = ruby_port.slave
else:
    MemClass = Simulation.setMemClass(options)
    system1.membus = SystemXBar()
    system1.system_port = system1.membus.slave
    CacheConfig.config_cache(options, system1)
    MemConfig.config_mem(options, system1)

root = Root(full_system = False, system1 = system1, system2 = system2)
m5.instantiate()

#map takes size in int, hence 0x100 = 256
#for i in xrange(np):
#    root.system1.cpu[i].workload[0].map(0xAAAAAA0000, 0xAAAAAA0000, 256)

#root.system1.cpu[0].workload[0].map(0xAAAAAA0000, 0xAAAAAA0000, 256)
print "**********Beginning simulation***********"

root.system2.cpu.workload[0].suspendthread()
for i in xrange(np):
    root.system1.cpu[i].workload[0].suspendthread()
#################################***Tiles***#############################################
m5.simulate(10000000)
return_object = system2.sys_conf.requestTile(4, 2)
tileId = return_object.pop()
if tileId != -1 and tileId != -2:
    core_ids = return_object
    
    for core in core_ids:
        root.system1.cpu[core].setTileId(tileId)
        root.system1.cpu[core].flushTLBs()
        #root.system1.cpu[core].workload[0].setTileId(tileId)
        #root.system1.cpu[core].workload[0].initState()
        #root.system1.cpu[core].workload[0].activatethread()
    
    process1.setTileId(tileId)
    process1.initState()
    process1.map(0xAAAAAA0000, 0xAAAAAA0000, 0x40000)
    process1.activatethread()
    #process2.setTileId(tileId)
    #process2.initState()
    #process2.map(0xAAAAAA0000, 0xAAAAAA0000, 0x40000)
    #process2.activatethread()
    #process3.setTileId(tileId)
    #process3.initState()
    #process3.map(0xAAAAAA0000, 0xAAAAAA0000, 0x40000)
    #process3.activatethread()
    #process4.setTileId(tileId)
    #process4.initState()
    #process4.map(0xAAAAAA0000, 0xAAAAAA0000, 0x40000)
    #process4.activatethread()
#m5.simulate(200000000000)
'''
return_object = system2.sys_conf.requestTile(4, 1)
tileId = return_object.pop()
if tileId != -1 and tileId != -2:
    core_ids = return_object
    
    for core in core_ids:
        root.system1.cpu[core].setTileId(tileId)
        root.system1.cpu[core].flushTLBs()
        #root.system1.cpu[core].workload[0].setTileId(tileId)
        #root.system1.cpu[core].workload[0].initState()
        #root.system1.cpu[core].workload[0].map(0xAAAAAA0000, 0xAAAAAA0000, 0x40000)
        #root.system1.cpu[core].workload[0].activatethread()
    
    process5.setTileId(tileId)
    process5.initState()
    process5.map(0xAAAAAA0000, 0xAAAAAA0000, 0x40000)
    process5.activatethread()
    process6.setTileId(tileId)
    process6.initState()
    process6.map(0xAAAAAA0000, 0xAAAAAA0000, 0x40000)
    process6.activatethread()
    process7.setTileId(tileId)
    process7.initState()
    process7.map(0xAAAAAA0000, 0xAAAAAA0000, 0x40000)
    process7.activatethread()
    #process8.setTileId(tileId)
    #process8.initState()
    #process8.map(0xAAAAAA0000, 0xAAAAAA0000, 0x40000)
    #process8.activatethread()

#m5.simulate(300000000000)
#system1.piobus.io_arbiter.setnewfifosize(0, 2)
#m5.simulate(200000000000)
#system1.piobus.io_arbiter.setnewfifosize(1, 2)

return_object = system2.sys_conf.requestTile(4, 4)
tileId = return_object.pop()
if tileId != -1 and tileId != -2:
    core_ids = return_object
    
    for core in core_ids:
        root.system1.cpu[core].setTileId(tileId)
        root.system1.cpu[core].flushTLBs()
        #root.system1.cpu[core].workload[0].setTileId(tileId)
        #root.system1.cpu[core].workload[0].initState()
        #root.system1.cpu[core].workload[0].map(0xAAAAAA0000, 0xAAAAAA0000, 0x40000)
        #root.system1.cpu[core].workload[0].activatethread()
    process3.setTileId(tileId)
    process3.initState()
    process3.map(0xAAAAAA0000, 0xAAAAAA0000, 0x40000)
    process3.activatethread()
    #process9.setTileId(tileId)
    #process9.initState()
    #process9.map(0xAAAAAA0000, 0xAAAAAA0000, 0x40000)
    #process9.activatethread()
    #process10.setTileId(tileId)
    #process10.initState()
    #process10.map(0xAAAAAA0000, 0xAAAAAA0000, 0x40000)
    #process10.activatethread()
    #process11.setTileId(tileId)
    #process11.initState()
    #process11.map(0xAAAAAA0000, 0xAAAAAA0000, 0x40000)
    #process11.activatethread()
    #process12.setTileId(tileId)
    #process12.initState()
    #process12.map(0xAAAAAA0000, 0xAAAAAA0000, 0x40000)
    #process12.activatethread()

#m5.simulate(30000000)
return_object = system2.sys_conf.requestTile(4, 4)
tileId = return_object.pop()
if tileId != -1 and tileId != -2:
    core_ids = return_object
    
    for core in core_ids:
        root.system1.cpu[core].setTileId(tileId)
        root.system1.cpu[core].flushTLBs()
        #root.system1.cpu[core].workload[0].setTileId(tileId)
        #root.system1.cpu[core].workload[0].initState()
        #root.system1.cpu[core].workload[0].map(0xAAAAAA0000, 0xAAAAAA0000, 0x40000)
        #root.system1.cpu[core].workload[0].activatethread()
    process4.setTileId(tileId)
    process4.initState()
    process4.map(0xAAAAAA0000, 0xAAAAAA0000, 0x40000)
    process4.activatethread()
    #process13.setTileId(tileId)
    #process13.initState()
    #process13.map(0xAAAAAA0000, 0xAAAAAA0000, 0x40000)
    #process13.activatethread()
    #process14.setTileId(tileId)
    #process14.initState()
    #process14.map(0xAAAAAA0000, 0xAAAAAA0000, 0x40000)
    #process14.activatethread()
    #process15.setTileId(tileId)
    #process15.initState()
    #process15.map(0xAAAAAA0000, 0xAAAAAA0000, 0x40000)
    #process15.activatethread()
    #process16.setTileId(tileId)
    #process16.initState()
    #process16.map(0xAAAAAA0000, 0xAAAAAA0000, 0x40000)
    #process16.activatethread()
'''
#########################################################################################
#while(1):
exit_event = m5.simulate()
#    if exit_event.getCause() == "vmexit":
#        TileId = system1.getexitTileId()
#        tile_processes[TileId] = tile_processes[TileId] - 1
#        if tile_processes[TileId] == 0:
#            print "Exiting TileId:",TileId
#            cores = system2.sys_conf.releaseTile(TileId)
#            for core in cores:
#                root.system1.cpu[core].setTileId(-1)
#            root.system1.cpu[core].workload[0].suspendthread()
#    else:
#        break
'''    msg = ''
    try:
        msg = queue.get(block = True, timeout = 0.01)

    except Exception as e:
        pass

    if msg == 'vmrequest':
        num_cores = queue.get()
        return_object = system2.sys_conf.requestTile(num_cores)
        tileId = return_object.pop()
    
        if tileId == -1:
            queue.put("Invalid number of cores!!!")
        elif tileId == -2:
            queue.put("No resources available!!!")
        else:
            core_ids = return_object
            for core in core_ids:
                root.system1.cpu[core].setTileId(tileId)
                root.system1.cpu[core].flushTLBs()
                root.system1.cpu[core].workload[0].setTileId(tileId)
                root.system1.cpu[core].workload[0].activatethread()
            root.system1.cpu[core_ids[0]].workload[0].initState()
            #tileId += 1
            queue.put("vmrequest acknowledged and tile successfully created :)", timeout = 2)

    elif msg == 'exit':
        break
'''
print 'Exiting @ tick %i because %s' % (m5.curTick(), exit_event.getCause())
