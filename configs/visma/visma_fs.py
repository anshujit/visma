# Copyright (c) 2010-2013, 2016 ARM Limited
# All rights reserved.
#
# The license below extends only to copyright in the software and shall
# not be construed as granting a license to any other intellectual
# property including but not limited to intellectual property relating
# to a hardware implementation of the functionality of the software
# licensed hereunder.  You may use the software subject to the license
# terms below provided that you ensure that this notice is replicated
# unmodified and in its entirety in all distributions of the software,
# modified or unmodified, in source code or in binary form.
#
# Copyright (c) 2012-2014 Mark D. Hill and David A. Wood
# Copyright (c) 2009-2011 Advanced Micro Devices, Inc.
# Copyright (c) 2006-2007 The Regents of The University of Michigan
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met: redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer;
# redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution;
# neither the name of the copyright holders nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# Authors: Ali Saidi
#          Brad Beckmann

import optparse
import sys
import pdb

import m5
from m5.defines import buildEnv
from m5.objects import *
from m5.util import addToPath, fatal

addToPath('../')

from ruby import Ruby

from common.FSConfig import *
from common.SysPaths import *
from common.Benchmarks import *
from common import Simulation
from common import CacheConfig
from common import MemConfig
from common.Caches import *
from common import Options


# Check if KVM support has been enabled, we might need to do VM
# configuration if that's the case.
have_kvm_support = 'BaseKvmCPU' in globals()
def is_kvm_cpu(cpu_class):
    return have_kvm_support and cpu_class != None and \
        issubclass(cpu_class, BaseKvmCPU)

def cmd_line_template():
    if options.command_line and options.command_line_file:
        print "Error: --command-line and --command-line-file are " \
              "mutually exclusive"
        sys.exit(1)
    if options.command_line:
        return options.command_line
    if options.command_line_file:
        return open(options.command_line_file).read().strip()
    return None

def build_test_system(np, bm, piobus=None):
    cmdline = cmd_line_template()
    if buildEnv['TARGET_ISA'] == "x86":
        (test_sys, base_entries, ext_entries) = makeLinuxX86System(test_mem_mode, np, bm,
                options.ruby, cmdline=cmdline, piobus=piobus)
    else:
        fatal("Incapable of building %s full system!", buildEnv['TARGET_ISA'])

    # Set the cache line size for the entire system
    test_sys.cache_line_size = options.cacheline_size

    # Create a top-level voltage domain
    test_sys.voltage_domain = VoltageDomain(voltage = options.sys_voltage)

    # Create a source clock for the system and set the clock period
    test_sys.clk_domain = SrcClockDomain(clock =  options.sys_clock,
            voltage_domain = test_sys.voltage_domain)

    # Create a CPU voltage domain
    test_sys.cpu_voltage_domain = VoltageDomain()

    # Create a source clock for the CPUs and set the clock period
    test_sys.cpu_clk_domain = SrcClockDomain(clock = options.cpu_clock,
                                             voltage_domain =
                                             test_sys.cpu_voltage_domain)

    if options.kernel is not None:
        test_sys.kernel = binary(options.kernel)

    if options.script is not None:
        test_sys.readfile = options.script

    if options.lpae:
        test_sys.have_lpae = True

    if options.virtualisation:
        test_sys.have_virtualization = True

    test_sys.init_param = options.init_param

    # For now, assign all the CPUs to the same clock domain
    test_sys.cpu = [TestCPUClass(clk_domain=test_sys.cpu_clk_domain, cpu_id=i)
                    for i in xrange(np)]

    if is_kvm_cpu(TestCPUClass) or is_kvm_cpu(FutureClass):
        test_sys.vm = KvmVM()

    return (test_sys, base_entries, ext_entries)

def build_drive_system(np, bm):
    # driver system CPU is always simple, so is the memory
    # Note this is an assignment of a class, not an instance.
    DriveCPUClass = AtomicSimpleCPU
    drive_mem_mode = 'atomic'
    DriveMemClass = SimpleMemory

    cmdline = cmd_line_template()
    if buildEnv['TARGET_ISA'] == 'alpha':
        drive_sys = makeLinuxAlphaSystem(drive_mem_mode, bm, cmdline=cmdline)
    elif buildEnv['TARGET_ISA'] == 'mips':
        drive_sys = makeLinuxMipsSystem(drive_mem_mode, bm, cmdline=cmdline)
    elif buildEnv['TARGET_ISA'] == 'sparc':
        drive_sys = makeSparcSystem(drive_mem_mode, bm, cmdline=cmdline)
    elif buildEnv['TARGET_ISA'] == 'x86':
        (drive_sys, base_entries, ext_entries) = makeLinuxX86System(drive_mem_mode, np, bm,
                                       cmdline=cmdline)
    elif buildEnv['TARGET_ISA'] == 'arm':
        drive_sys = makeArmSystem(drive_mem_mode, options.machine_type, np,
                                  bm, options.dtb_filename, cmdline=cmdline)

    pci_dev5_inta = X86IntelMPIOIntAssignment(
            interrupt_type = 'INT',
            polarity = 'ConformPolarity',
            trigger = 'ConformTrigger',
            source_bus_id = 0,
            source_bus_irq = 0 + (5 << 2),
            dest_io_apic_id = drive_sys.pc.south_bridge.io_apic.apic_id,
            dest_io_apic_intin = 10)
    base_entries.append(pci_dev5_inta)

    drive_sys.intel_mp_table.base_entries = base_entries
    drive_sys.intel_mp_table.ext_entries = ext_entries

    # Create a top-level voltage domain
    drive_sys.voltage_domain = VoltageDomain(voltage = options.sys_voltage)

    # Create a source clock for the system and set the clock period
    drive_sys.clk_domain = SrcClockDomain(clock =  options.sys_clock,
            voltage_domain = drive_sys.voltage_domain)

    # Create a CPU voltage domain
    drive_sys.cpu_voltage_domain = VoltageDomain()

    # Create a source clock for the CPUs and set the clock period
    drive_sys.cpu_clk_domain = SrcClockDomain(clock = options.cpu_clock,
                                              voltage_domain =
                                              drive_sys.cpu_voltage_domain)

    drive_sys.cpu = DriveCPUClass(clk_domain=drive_sys.cpu_clk_domain,
                                  cpu_id=0)
    drive_sys.cpu.createThreads()
    drive_sys.cpu.createInterruptController()
    drive_sys.cpu.connectAllPorts(drive_sys.membus)
    if options.fastmem:
        drive_sys.cpu.fastmem = True
    if options.kernel is not None:
        drive_sys.kernel = binary(options.kernel)

    if is_kvm_cpu(DriveCPUClass):
        drive_sys.vm = KvmVM()

    drive_sys.iobridge = Bridge(delay='50ns',
                                ranges = drive_sys.mem_ranges)
    drive_sys.iobridge.slave = drive_sys.iobus.master
    drive_sys.iobridge.master = drive_sys.membus.slave

    # Create the appropriate memory controllers and connect them to the
    # memory bus
    drive_sys.mem_ctrls = [DriveMemClass(range = r)
                           for r in drive_sys.mem_ranges]
    for i in xrange(len(drive_sys.mem_ctrls)):
        drive_sys.mem_ctrls[i].port = drive_sys.membus.master

    drive_sys.init_param = options.init_param

    if options.script2 is not None:
        drive_sys.readfile = options.script2

    return drive_sys

# Add options
parser = optparse.OptionParser()
Options.addCommonOptions(parser)
Options.addFSOptions(parser)

# Add the ruby specific and protocol specific options
if '--ruby' in sys.argv:
    Ruby.define_options(parser)

(options, args) = parser.parse_args()

if args:
    print "Error: script doesn't take any positional arguments"
    sys.exit(1)

# system under test can be any CPU
(TestCPUClass, test_mem_mode, FutureClass) = Simulation.setCPUClass(options)

# Match the memories with the CPUs, based on the options for the test system
TestMemClass = Simulation.setMemClass(options)

num_vms = options.num_vms
num_cpuspervm = options.num_cpuspervm
vm_cpus = []
systems = []
vm_mems = []
total_num_cpus = 0
total_mem_size = MemorySize('0B')
bm = []
sys_base_entries = []
sys_ext_entries = []

for i in xrange(num_vms):
    vm_cpus.append(options.num_cpuspervm)
    vm_mems.append(options.mem_size)
    bm.append(SysConfig(disk=options.disk_image, mem=vm_mems[i], rootdev=options.root_device,
                        os_type=options.os_type))
    if i == 0:
        (system, base_entries, ext_entries) = build_test_system(vm_cpus[i], bm[i])
        systems.append(system)
    else:
        (system, base_entries, ext_entries) = build_test_system(vm_cpus[i], bm[i], systems[0].iobus)
        systems.append(system)
    
    sys_base_entries.append(base_entries)
    sys_ext_entries.append(ext_entries)

    #systems[i].system_id = i
    total_num_cpus = total_num_cpus + vm_cpus[i]
    total_mem_size.value = total_mem_size.value + MemorySize(vm_mems[i]).value

systems[0].device_virtualizer = DeviceVirtualizer(cycle_period = 1000000000)
systems[0].dma_virtualizer = DmaVirtualizer()
systems[0].device_virtualizer.dmavirt = systems[0].dma_virtualizer
maxcontrolpkts = [50, 100]
maxdmapkts = [50, 100]
systems[0].device_virtualizer.maxcontrolpkts = maxcontrolpkts
systems[0].dma_virtualizer.maxdmapkts = maxdmapkts

systems[0].device_virtualizer.slave = systems[0].iobus.master

disks = []

for (j, vm) in enumerate(systems):
    vm.pc.south_bridge.ide.tileID = j
    vm.pc.south_bridge.pcioffset = PciOffset(tileID = j)
    vm.pc.south_bridge.ide.pio = vm.pc.south_bridge.pcioffset.io_port
    #vm.pc.south_bridge.pcioffset.bus_port = systems[0].iobus.master
    vm.pc.south_bridge.pcioffset.bus_port = systems[0].device_virtualizer.master

    for disk in vm.pc.south_bridge.ide.disks:
        disk.tileID = j
        disk.dma_offset = j * MemorySize(vm_mems[i]).value
        disks.append(disk)

    for cpu in vm.cpu:
        cpu.tileID = j

systems[0].pc.south_bridge.ide.disks[0].maxbytecount = 2048
systems[0].pc.south_bridge.ide.disks[1].maxbytecount = 2048
systems[1].pc.south_bridge.ide.disks[0].maxbytecount = 4096
systems[1].pc.south_bridge.ide.disks[1].maxbytecount = 4096
maxdiskbytes = [systems[0].pc.south_bridge.ide.disks[0].maxbytecount, systems[0].pc.south_bridge.ide.disks[1].maxbytecount,
                systems[1].pc.south_bridge.ide.disks[0].maxbytecount, systems[1].pc.south_bridge.ide.disks[1].maxbytecount]
systems[0].device_virtualizer.maxdiskbytes = maxdiskbytes
systems[0].device_virtualizer.disks = disks

for (j, vm) in enumerate(systems):
    if j != 0:
        vm.pc.south_bridge.ide.host = systems[0].pc.pci_host
        systems[0].pc.pci_host.platform2 = vm.pc
#print "system0 mem_range:",systems[0].mem_ranges[0]
#print "system1 mem_range:",systems[1].mem_ranges[0]

if options.dual:
    bm.append(SysConfig(disk=options.disk_image, rootdev=options.root_device, mem=options.mem_size, os_type=options.os_type))
    drive_system = build_drive_system(1, bm[-1])

    drive_system.pc.south_bridge.ide.pio = drive_system.iobus.master
    for disk in drive_system.pc.south_bridge.ide.disks:
        disk.tileID = 0
    
    systems[0].pc.ethernet = IGbE_e1000(pci_bus=0, pci_dev=5, pci_func=0,
                                     InterruptLine=10, InterruptPin=1)
    systems[0].pc.ethernet.tileID = 0
    
    pci_dev5_inta = X86IntelMPIOIntAssignment(
            interrupt_type = 'INT',
            polarity = 'ConformPolarity',
            trigger = 'ConformTrigger',
            source_bus_id = 0,
            source_bus_irq = 0 + (5 << 2),
            dest_io_apic_id = systems[0].pc.south_bridge.io_apic.apic_id,
            dest_io_apic_intin = 10)
    sys_base_entries[0].append(pci_dev5_inta)
    
    systems[0].pc.ethernet.host = systems[0].pc.pci_host
    systems[0].pc.ethernet.pio = systems[0].iobus.master
    systems[0]._dma_ports.append(systems[0].pc.ethernet.dma)

    drive_system.pc.ethernet = IGbE_e1000(pci_bus=0, pci_dev=5, pci_func=0, 
                                        InterruptLine=10, InterruptPin=1)
    drive_system.pc.ethernet.pio = drive_system.iobus.master
    drive_system.pc.ethernet.dma = drive_system.iobus.slave
    drive_system.pc.ethernet.host = drive_system.pc.pci_host
    
for (i, vm) in enumerate(systems):
    vm.intel_mp_table.base_entries = sys_base_entries[i]
    vm.intel_mp_table.ext_entries = sys_ext_entries[i]

if options.ruby:
    # Check for timing mode because ruby does not support atomic accesses
    if not (options.cpu_type == "detailed" or options.cpu_type == "timing"):
        print >> sys.stderr, "Ruby requires TimingSimpleCPU or O3CPU!!"
        sys.exit(1)
    ruby = Ruby.create_vsystem(options, systems, total_num_cpus, total_mem_size, vm_cpus, vm_mems)

    #systems[0].device_virtualizer.offsets = offsets
    # Create a seperate clock domain for Ruby
    ruby.clk_domain = SrcClockDomain(clock = options.ruby_clock,
                                    voltage_domain = systems[0].voltage_domain)

    # Connect the ruby io port to the PIO bus,
    # assuming that there is just one such port.
    for (j, vm) in enumerate(systems):
        #exec("vm.iobus.master = ruby._io_port%d.slave" % j)
        exec("systems[0].iobus.master = ruby._io_port%d.slave" % j)
    k = 0
    for (j, vm) in enumerate(systems):
        for cpu in vm.cpu:
            #
            # Tie the cpu ports to the correct ruby system ports
            #
            cpu.clk_domain = vm.cpu_clk_domain
            cpu.createThreads()
            cpu.createInterruptController()

            cpu.icache_port = ruby._cpu_ports[k].slave
            cpu.dcache_port = ruby._cpu_ports[k].slave

            if buildEnv['TARGET_ISA'] in ("x86", "arm"):
                cpu.itb.walker.port = ruby._cpu_ports[k].slave
                cpu.dtb.walker.port = ruby._cpu_ports[k].slave

            if buildEnv['TARGET_ISA'] in "x86":
                cpu.interrupts[0].pio = ruby._cpu_ports[k].master
                cpu.interrupts[0].int_master = ruby._cpu_ports[k].slave
                cpu.interrupts[0].int_slave = ruby._cpu_ports[k].master
            
            k += 1

#else:
#    print "ViSMA is only designed with the Ruby memory model"
#    sys.exit(1)

if len(bm) == 1 and options.dist:
    # This system is part of a dist-gem5 simulation
    root = makeDistRoot(test_sys,
                        options.dist_rank,
                        options.dist_size,
                        options.dist_server_name,
                        options.dist_server_port,
                        options.dist_sync_repeat,
                        options.dist_sync_start,
                        options.ethernet_linkspeed,
                        options.ethernet_linkdelay,
                        options.etherdump);

for (j, vm) in enumerate(systems):
    vm.pc.south_bridge.io_apic.tileID = j

    #for i in xrange(len(vm.cpu)):
    #    vm.cpu[i].interrupts[0].tileID = j

root = Root(full_system=True, system=systems[0])

for (i, vm) in enumerate(systems):
   if i != 0:
       exec("root.system%d = systems[i]" % i)

if options.dual:
    exec("root.system%d = drive_system" % len(systems))
    root.etherlink = EtherLink()
    root.etherlink.int0 = systems[0].pc.ethernet.interface
    root.etherlink.int1 = drive_system.pc.ethernet.interface

if options.timesync:
    root.time_sync_enable = True

if options.frame_capture:
    VncServer.frame_capture = True

# instantiate all the simobjects we have created
m5.instantiate()

#########################Set up the LUTs#########################
'''
for k in xrange(total_num_cpus):
    for i in xrange(total_num_cpus):
        if k < 4:
            if i < 4:
                systems[0].ruby.network.netifs[k].setLUTbit(i)
            else:
                systems[0].ruby.network.netifs[k].resetLUTbit(i)

            systems[0].ruby.network.netifs[k].resetLUTdmabit(3)
        else:
            if i < 4:
                systems[0].ruby.network.netifs[k].resetLUTbit(i)
            else:
                systems[0].ruby.network.netifs[k].setLUTbit(i)

            systems[0].ruby.network.netifs[k].resetLUTdmabit(2)
'''
#################################################################
# Start simulation
print "**************Starting Simulation**************"
exit_event = m5.simulate()
print 'Exiting @ tick %i because %s' % (m5.curTick(), exit_event.getCause())
