import m5
from m5.defines import buildEnv
from m5.objects import *
from m5.util import addToPath, fatal

def create_subsystem(system, index, core_ids):
    if index == 0:
        system.subsystem = [SubSystem()]
    else:
        system.subsystem.append(SubSystem())

    system.subsystem[index].cpu = [system.cpu[x] for x in core_ids]
    system.subsystem[index].mem_ctrls = [system.mem_ctrls[x] for x in core_ids]

    for x in range(len(system.subsystem[index].cpu)):
        print [system.subsystem[index].mem_ctrls[x]]

    binary = 'tests/test-progs/my_test/huge'
    process = LiveProcess()
    process.cmd = [binary]
    for i in range(len(system.subsystem[index].cpu)):
        system.subsystem[index].cpu[i].workload = process
        system.subsystem[index].cpu[i].createThreads()
