# import the m5 (gem5) library created when gem5 is built
import m5
import sys

from multiprocessing.managers import BaseManager
import Queue
import time
# import all of the SimObjects
from m5.objects import *

system2 = System()

queue = Queue.Queue()
class QueueManager(BaseManager): pass
QueueManager.register('get_queue', callable=lambda:queue)
m = QueueManager(address=('', 50000), authkey='abc')

system2.clk_domain = SrcClockDomain()
system2.clk_domain.clock = '1GHz'
system2.clk_domain.voltage_domain = VoltageDomain()

# Set up the system
system2.mem_mode = 'timing'               # Use timing accesses
system2.mem_ranges = [AddrRange('512MB')] # Create an address range

# Create a simple CPU
system2.cpu = TimingSimpleCPU(cpu_id = 0)

# Create a memory bus, a system crossbar, in this case
system2.membus = SystemXBar()

# Hook the CPU ports up to the membus
system2.cpu.icache_port = system2.membus.slave
system2.cpu.dcache_port = system2.membus.slave

# create the interrupt controller for the CPU and connect to the membus
system2.cpu.createInterruptController()

# For x86 only, make sure the interrupts are connected to the memory
# Note: these are directly connected to the memory bus and are not cached
if m5.defines.buildEnv['TARGET_ISA'] == "x86":
    system2.cpu.interrupts[0].pio = system2.membus.master
    system2.cpu.interrupts[0].int_master = system2.membus.slave
    system2.cpu.interrupts[0].int_slave = system2.membus.master

# Create a DDR3 memory controller and connect it to the membus
system2.mem_ctrl = DDR3_1600_x64()
system2.mem_ctrl.range = system2.mem_ranges[0]
system2.mem_ctrl.port = system2.membus.master

# Connect the system up to the membus
system2.system_port = system2.membus.slave

# get ISA for the binary to run.
isa = str(m5.defines.buildEnv['TARGET_ISA']).lower()

# Run 'hello' and use the compiled ISA to find the binary
#binary = 'tests/test-progs/hello/bin/' + isa + '/linux/hello'
binary = 'tests/test-progs/my_test/vm'

# Create a process for a simple "Hello World" application
process2 = LiveProcess()
# Set the command
# cmd is a list which begins with the executable (like argv)
process2.cmd = [binary]
# Set the cpu to use the process as its workload and create thread contexts
system2.cpu.workload = process2
system2.cpu.createThreads()
#################################################################

root = Root(full_system = False, system = system2)
m5.instantiate()
print "**********Beginning simulation**********"
m.start()
queue = m.get_queue()

while (1):
    exit_event = m5.simulate()
    if exit_event.getCause() == 'vmrequest':     
        queue.put('vmrequest')
        num_cores = input("Enter the no. cores:")
        queue.put(num_cores)
        try:
            msg = queue.get()
            print msg
        except Exception as e:
            print "Request Failed :("
            pass    
    else:
        queue.put('exit')
        break

m.shutdown()
print 'Exiting @ tick %i because %s' % (m5.curTick(), exit_event.getCause())
