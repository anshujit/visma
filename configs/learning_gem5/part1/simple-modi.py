# -*- coding: utf-8 -*-
# Copyright (c) 2015 Jason Power
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met: redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer;
# redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution;
# neither the name of the copyright holders nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# Authors: Jason Power

""" This file creates a barebones system and executes 'hello', a simple Hello
World application.
See Part 1, Chapter 2: Creating a simple configuration script in the
learning_gem5 book for more information about this script.

IMPORTANT: If you modify this file, it's likely that the Learning gem5 book
           also needs to be updated. For now, email Jason <power.jg@gmail.com>

"""

# import the m5 (gem5) library created when gem5 is built
import m5
# import all of the SimObjects
from m5.objects import *

# create the system we are going to simulate
system1 = System()

# Set the clock fequency of the system (and all of its children)
system1.clk_domain = SrcClockDomain()
system1.clk_domain.clock = '1GHz'
system1.clk_domain.voltage_domain = VoltageDomain()

# Set up the system
system1.mem_mode = 'timing'               # Use timing accesses
system1.mem_ranges = [AddrRange('512MB')] # Create an address range

# Create a simple CPU
system1.cpu = TimingSimpleCPU(cpu_id = 0)

# Create a memory bus, a system crossbar, in this case
system1.membus = SystemXBar()

# Hook the CPU ports up to the membus
system1.cpu.icache_port = system1.membus.slave
system1.cpu.dcache_port = system1.membus.slave

# create the interrupt controller for the CPU and connect to the membus
system1.cpu.createInterruptController()

# For x86 only, make sure the interrupts are connected to the memory
# Note: these are directly connected to the memory bus and are not cached
if m5.defines.buildEnv['TARGET_ISA'] == "x86":
    system1.cpu.interrupts[0].pio = system1.membus.master
    system1.cpu.interrupts[0].int_master = system1.membus.slave
    system1.cpu.interrupts[0].int_slave = system1.membus.master

# Create a DDR3 memory controller and connect it to the membus
system1.mem_ctrl = DDR3_1600_x64()
system1.mem_ctrl.range = system1.mem_ranges[0]
system1.mem_ctrl.port = system1.membus.master

# Connect the system up to the membus
system1.system_port = system1.membus.slave

# get ISA for the binary to run.
isa = str(m5.defines.buildEnv['TARGET_ISA']).lower()

# Run 'hello' and use the compiled ISA to find the binary
#binary2 = 'tests/test-progs/hello/bin/' + isa2 + '/linux/hello'
#binary = 'tests/test-progs/hello/bin/' + isa + '/linux/hello'
binary = 'tests/test-progs/my_test/hello1'
binary2 = 'tests/test-progs/my_test/hello2'
#binary2 = 'tests/test-progs/hello/bin/' + isa + '/linux/hello'

process = LiveProcess()
# Create a process for a simple "Hello World" application
process2 = LiveProcess()
# Set the command
# cmd is a list which begins with the executable (like argv)
process.cmd = [binary]
process2.cmd = [binary2]

# Set the cpu to use the process as its workload and create thread contexts
system1.cpu.workload = process

system1.cpu.createThreads()
###########################################################
# set up the root SimObject and start the simulation
root = Root(full_system = False, system1 = system1)
# instantiate all of the objects we've created above
m5.instantiate()

print "Beginning simulation!"
exit_event = m5.simulate()
#system1.cpu.workload = process2
system1.cpu.switchworkload(process2)
exit_event = m5.simulate()
print 'Exiting @ tick %i because %s' % (m5.curTick(), exit_event.getCause())
