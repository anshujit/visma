# Copyright (c) 2012 ARM Limited
# All rights reserved.
#
# The license below extends only to copyright in the software and shall
# not be construed as granting a license to any other intellectual
# property including but not limited to intellectual property relating
# to a hardware implementation of the functionality of the software
# licensed hereunder.  You may use the software subject to the license
# terms below provided that you ensure that this notice is replicated
# unmodified and in its entirety in all distributions of the software,
# modified or unmodified, in source code or in binary form.
#
# Copyright (c) 2006-2007 The Regents of The University of Michigan
# Copyright (c) 2009 Advanced Micro Devices, Inc.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met: redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer;
# redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution;
# neither the name of the copyright holders nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# Authors: Brad Beckmann

import math
import m5

from m5.objects import *
from m5.defines import buildEnv
from m5.util import addToPath, fatal

from common import MemConfig

from topologies import *
from network import Network

def define_options(parser):
    # By default, ruby uses the simple timing cpu
    parser.set_defaults(cpu_type="timing")

    parser.add_option("--ruby-clock", action="store", type="string",
                      default='2GHz',
                      help="Clock for blocks running at Ruby system's speed")

    parser.add_option("--access-backing-store", action="store_true", default=False,
                      help="Should ruby maintain a second copy of memory")

    # Options related to cache structure
    parser.add_option("--ports", action="store", type="int", default=4,
                      help="used of transitions per cycle which is a proxy \
                            for the number of ports.")

    # network options are in network/Network.py

    # ruby mapping options
    parser.add_option("--numa-high-bit", type="int", default=0,
                      help="high order address bit to use for numa mapping. " \
                           "0 = highest bit, not specified = lowest bit")

    parser.add_option("--recycle-latency", type="int", default=10,
                      help="Recycle latency for ruby controller input buffers")

    protocol = buildEnv['PROTOCOL']
    exec "import %s" % protocol
    eval("%s.define_options(parser)" % protocol)
    Network.define_options(parser)

def setup_memory_controllers(system, ruby, dir_cntrls, options):
    ruby.block_size_bytes = options.cacheline_size
    ruby.memory_size_bits = 48
    block_size_bits = int(math.log(options.cacheline_size, 2))

    if options.numa_high_bit:
        numa_bit = options.numa_high_bit
    else:
        # if the numa_bit is not specified, set the directory bits as the
        # lowest bits above the block offset bits, and the numa_bit as the
        # highest of those directory bits
        dir_bits = int(math.log(options.num_dirs, 2))
        numa_bit = block_size_bits + dir_bits - 1

    index = 0
    mem_ctrls = []
    crossbars = []

    # Sets bits to be used for interleaving.  Creates memory controllers
    # attached to a directory controller.  A separate controller is created
    # for each address range as the abstract memory can handle only one
    # contiguous address range as of now.
    
    for dir_cntrl in dir_cntrls:
        dir_cntrl.directory.numa_high_bit = numa_bit

        crossbar = None
        if len(system.mem_ranges) > 1:
            crossbar = IOXBar()
            crossbars.append(crossbar)
            dir_cntrl.memory = crossbar.slave

        for r in system.mem_ranges:
            print "mem_ranges type:", type(r)
            mem_ctrl = MemConfig.create_mem_ctrl(
                MemConfig.get(options.mem_type), r, index, options.num_dirs,
                int(math.log(options.num_dirs, 2)), options.cacheline_size)

            if options.access_backing_store:
                mem_ctrl.kvm_map=False

            mem_ctrls.append(mem_ctrl)

            if crossbar != None:
                mem_ctrl.port = crossbar.master
            else:
                mem_ctrl.port = dir_cntrl.memory

        index += 1

    system.mem_ctrls = mem_ctrls

    if len(crossbars) > 0:
        ruby.crossbars = crossbars

def setup_vmemory_controllers(systems, ruby, dir_cntrls, options, total_mem_size):
    ruby.block_size_bytes = options.cacheline_size
    ruby.memory_size_bits = 48
    block_size_bits = int(math.log(options.cacheline_size, 2))

    if options.numa_high_bit:
        numa_bit = options.numa_high_bit
    else:
        # if the numa_bit is not specified, set the directory bits as the
        # lowest bits above the block offset bits, and the numa_bit as the
        # highest of those directory bits
        dir_bits = int(math.log(options.num_dirs, 2))
        numa_bit = block_size_bits + dir_bits - 1

    index = 0
    mem_ctrls = []
    crossbars = []

    # Sets bits to be used for interleaving.  Creates memory controllers
    # attached to a directory controller.  A separate controller is created
    # for each address range as the abstract memory can handle only one
    # contiguous address range as of now.
    
    for dir_cntrl in dir_cntrls:
        dir_cntrl.directory.numa_high_bit = numa_bit

        crossbar = None
        if total_mem_size.value > MemorySize('3GB').value:
            print "ViSMA doesn't work with mem_size:",total_mem_size.value," (> 3GB)"
            sys.exit(1)

        #for r in system.mem_ranges:
        r = AddrRange(total_mem_size)
        #print "mem_ranges type:", type(r)
        mem_ctrl = MemConfig.create_mem_ctrl(
            MemConfig.get(options.mem_type), r, index, options.num_dirs,
            int(math.log(options.num_dirs, 2)), options.cacheline_size)

        if options.access_backing_store:
            mem_ctrl.kvm_map=False

        mem_ctrls.append(mem_ctrl)

        if crossbar != None:
            mem_ctrl.port = crossbar.master
        else:
            mem_ctrl.port = dir_cntrl.memory

        index += 1

    mem_ctrls0 = []
    mem_ctrls1 = []
    
    for i in xrange(len(mem_ctrls)):
        if i < options.num_cpuspervm:
            mem_ctrls0.append(mem_ctrls[i])
        else:
            mem_ctrls1.append(mem_ctrls[i])

    for (j, vm) in enumerate(systems):
        if j == 0:
            vm.mem_ctrls = mem_ctrls0
        else:
            vm.mem_ctrls = mem_ctrls1
    
    #systems[0].mem_ctrls = mem_ctrls0
    #systems[1].mem_ctrls = mem_ctrls1
    
    #ruby.mem_ctrls = mem_ctrls
    #systems[0].mem_ctrls = mem_ctrls
    #systems[1].memories = mem_ctrls

    if len(crossbars) > 0:
        ruby.crossbars = crossbars

def create_topology(controllers, options):
    """ Called from create_system in configs/ruby/<protocol>.py
        Must return an object which is a subclass of BaseTopology
        found in configs/topologies/BaseTopology.py
        This is a wrapper for the legacy topologies.
    """
    exec "import topologies.%s as Topo" % options.topology
    topology = eval("Topo.%s(controllers)" % options.topology)
    return topology

def create_system(options, full_system, system, piobus = None, dma_ports = []):

    system.ruby = RubySystem()
    ruby = system.ruby

    # Create the network object
    (network, IntLinkClass, ExtLinkClass, RouterClass, InterfaceClass) = \
        Network.create_network(options, ruby)
    
    ruby.network = network

    protocol = buildEnv['PROTOCOL']
    exec "import %s" % protocol
    try:
        (cpu_sequencers, dir_cntrls, topology) = \
             eval("%s.create_system(options, full_system, system, dma_ports,\
                                    ruby)"
                  % protocol)
    except:
        print "Error: could not create sytem for ruby protocol %s" % protocol
        raise

    # Create the network topology
    topology.makeTopology(options, network, IntLinkClass, ExtLinkClass,
            RouterClass)

    # Initialize network based on topology
    Network.init_network(options, network, InterfaceClass)

    # Create a port proxy for connecting the system port. This is
    # independent of the protocol and kept in the protocol-agnostic
    # part (i.e. here).
    sys_port_proxy = RubyPortProxy(ruby_system = ruby)
    if piobus is not None:
        sys_port_proxy.pio_master_port = piobus.slave

    # Give the system port proxy a SimObject parent without creating a
    # full-fledged controller
    system.sys_port_proxy = sys_port_proxy

    # Connect the system port for loading of binaries etc
    system.system_port = system.sys_port_proxy.slave

    setup_memory_controllers(system, ruby, dir_cntrls, options)
    #throttlers = []
    # Connect the cpu sequencers and the piobus
    if piobus != None:
        #i = 0
        for cpu_seq in cpu_sequencers:
            #throttler = Throttler()
            #throttlers.append(throttler)
            cpu_seq.pio_master_port = piobus.slave
            cpu_seq.mem_master_port = piobus.slave
            #throttler.mem_side = piobus.slave
            #i += 1
            #cpu_seq.mem_master_port = piobus.slave

            if buildEnv['TARGET_ISA'] == "x86":
                cpu_seq.pio_slave_port = piobus.master
    #system.throttlers = throttlers
    ruby.number_of_virtual_networks = ruby.network.number_of_virtual_networks
    ruby._cpu_ports = cpu_sequencers
    ruby.num_of_sequencers = len(cpu_sequencers)

    # Create a backing copy of physical memory in case required
    if options.access_backing_store:
        ruby.access_backing_store = True
        ruby.phys_mem = SimpleMemory(range=system.mem_ranges[0],
                                     in_addr_map=False)
    return network

def create_vsystem(options, systems, total_num_cpus, total_mem_size, vm_cpus, vm_mems):
    
    #ruby = RubySystem()
    #systems[0].ruby = ruby
    systems[0].ruby = RubySystem()
    ruby = systems[0].ruby

    # Create the network object
    (network, IntLinkClass, ExtLinkClass, RouterClass, InterfaceClass) = \
        Network.create_network(options, ruby)
    
    ruby.network = network

    protocol = buildEnv['PROTOCOL']
    exec "import %s" % protocol
    try:
        (cpu_sequencers, dir_cntrls, topology, num_dmas) = \
             eval("%s.create_vsystem(options, systems, ruby, total_num_cpus, total_mem_size, vm_cpus, vm_mems)"
                  % protocol)
    except:
        print "Error: could not create sytem for ruby protocol %s" % protocol
        raise

    for i in xrange(len(cpu_sequencers)):
        if i < options.num_cpuspervm:
            cpu_sequencers[i].tileID = 0
        else:
            cpu_sequencers[i].tileID = 1
        cpu_sequencers[i].offset = cpu_sequencers[i].tileID * MemorySize(vm_mems[0]).value
        
    # Create the network topology
    topology.makevTopology(options, network, IntLinkClass, ExtLinkClass,
            RouterClass, total_num_cpus, num_dmas)

    # Initialize network based on topology
    Network.init_network(options, network, InterfaceClass, num_dmas)

    # Create a port proxy for connecting the system port. This is
    # independent of the protocol and kept in the protocol-agnostic
    # part (i.e. here).
    for (i, vm) in enumerate(systems):
        sys_port_proxy = RubyPortProxy(ruby_system = ruby)
        #if vm.iobus is not None:
        sys_port_proxy.pio_master_port = systems[0].iobus.slave#vm.iobus.slave

        # Give the system port proxy a SimObject parent without creating a
        # full-fledged controller
        vm.sys_port_proxy = sys_port_proxy
        vm.sys_port_proxy.version = i

        # Connect the system port for loading of binaries etc
        vm.system_port = vm.sys_port_proxy.slave
        sys_port_proxy.tileID = i
        sys_port_proxy.offset = i * MemorySize(vm_mems[i]).value

    setup_vmemory_controllers(systems, ruby, dir_cntrls, options, total_mem_size)
    total_offsets = []
    # Connect the cpu sequencers and the piobus
    k = 0
    for (j, vm) in enumerate(systems):
        #if vm.iobus != None:
        intr_offsets = []
        intr_negoffsets = []
        for i in xrange(int(vm_cpus[j])):
            intr_offset = Offset(tileID = j)
            intr_negoffset = NegOffset(tileID = j)
            cpu_sequencers[k].mem_master_port = intr_offset.ruby_port
            intr_offset.io_port = systems[0].iobus.slave
            cpu_sequencers[k].pio_master_port = systems[0].iobus.slave#vm.iobus.slave
            #cpu_sequencers[k].mem_master_port = systems[0].iobus.slave#vm.iobus.slave
                #if options.dual:
                #    cpu_sequencers[k].pio_master_port2 = systems[0].iobus2.slave
                #    cpu_sequencers[k].mem_master_port2 = systems[0].iobus2.slave

            if buildEnv['TARGET_ISA'] == "x86":
                cpu_sequencers[k].pio_slave_port = intr_negoffset.ruby_port
                intr_negoffset.io_port = systems[0].iobus.master
                #cpu_sequencers[k].pio_slave_port = systems[0].iobus.master#vm.iobus.master
                #    if options.dual:
                #        cpu_sequencers[k].pio_slave_port2 = systems[0].iobus2.master
            k += 1
            total_offsets.append(intr_offset)
            intr_offsets.append(intr_offset)
            intr_negoffsets.append(intr_negoffset)
        vm.intr_offsets = intr_offsets
        vm.intr_negoffsets = intr_negoffsets
    
    systems[0].device_virtualizer.offsets = total_offsets

    ruby.number_of_virtual_networks = ruby.network.number_of_virtual_networks
    ruby._cpu_ports = cpu_sequencers
    ruby.num_of_sequencers = len(cpu_sequencers)

    # Create a backing copy of physical memory in case required
    if options.access_backing_store:
        ruby.access_backing_store = True
        ruby.phys_mem = SimpleMemory(range=AddrRange(total_mem_size),
                                     in_addr_map=False)
    return ruby

def send_evicts(options):
    # currently, 2 scenarios warrant forwarding evictions to the CPU:
    # 1. The O3 model must keep the LSQ coherent with the caches
    # 2. The x86 mwait instruction is built on top of coherence invalidations
    if options.cpu_type == "detailed" or buildEnv['TARGET_ISA'] == 'x86':
        return True
    return False
