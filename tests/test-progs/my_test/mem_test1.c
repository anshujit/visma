#include <stdio.h>
//#include <time.h>
//#include "m5op.h"
int main()
{
	int volatile * const a_reg = (int *) 0x00000000;
    int volatile * const b_reg = (int *) 0x10000000;
    int volatile * const c_reg = (int *) 0x20000000;
    int volatile * const d_reg = (int *) 0x30000000;
    int volatile * const e_reg = (int *) 0x40000000;
    int volatile * const f_reg = (int *) 0x50000000;
    int volatile * const g_reg = (int *) 0x60000000;
    int volatile * const h_reg = (int *) 0x70000000;
    int volatile * const i_reg = (int *) 0x80000000;
    int volatile * const j_reg = (int *) 0x90000000;
    int volatile * const k_reg = (int *) 0xA0000000;
    int volatile * const l_reg = (int *) 0xB0000000;
    int volatile * const m_reg = (int *) 0xC0000000;
    int volatile * const n_reg = (int *) 0xD0000000;
    int volatile * const o_reg = (int *) 0xE0000000;
    int volatile * const p_reg = (int *) 0xF0000000;

    *a_reg = 0x12;
    *b_reg = 0x12;
    *c_reg = 0x12;
    *d_reg = 0x12;
    *e_reg = 0x12;
    *f_reg = 0x12;
    *g_reg = 0x12;
    *h_reg = 0x12;
    *i_reg = 0x12;
    *j_reg = 0x12;
    *k_reg = 0x12;
    *l_reg = 0x12;
    *m_reg = 0x12;
    *n_reg = 0x12;
    *o_reg = 0x12;
    *p_reg = 0x12;
}
